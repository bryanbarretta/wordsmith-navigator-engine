package bryans.work.wordsmithnavigatorengine.util;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import bryans.work.wordsmithnavigatorengine.astar.AStar;
import bryans.work.wordsmithnavigatorengine.database.DatabaseGateway;
import bryans.work.wordsmithnavigatorengine.enums.Direction;
import bryans.work.wordsmithnavigatorengine.enums.Position;
import bryans.work.wordsmithnavigatorengine.enums.SpecialCharacters;
import bryans.work.wordsmithnavigatorengine.enums.TileCodes;
import bryans.work.wordsmithnavigatorengine.objects.HintLetter;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;

public class Util {

    public static int getPointsForWord(String word){
        int points = 0;
        for (Character c : word.toCharArray()){
            points += getPointsForLetter(c);
        }
        return points;
    }

    public static int getPointsForLetter(char l)
    {
        switch (Character.toUpperCase(l))
        {
            case 'A': return 1;
            case 'B': return 3;
            case 'C': return 3;
            case 'D': return 2;
            case 'E': return 1;
            case 'F': return 4;
            case 'G': return 2;
            case 'H': return 4;
            case 'I': return 1;
            case 'J': return 8;
            case 'K': return 5;
            case 'L': return 1;
            case 'M': return 3;
            case 'N': return 1;
            case 'O': return 1;
            case 'P': return 3;
            case 'Q': return 10;
            case 'R': return 1;
            case 'S': return 1;
            case 'T': return 1;
            case 'U': return 1;
            case 'V': return 4;
            case 'W': return 4;
            case 'X': return 8;
            case 'Y': return 4;
            case 'Z': return 10;
        }
        return 0;
    }


    public static Direction getOppositeDirection(Direction direction){
        if(direction.equals(Direction.SOUTH)){
            return Direction.NORTH;
        }
        if(direction.equals(Direction.NORTH)){
            return Direction.SOUTH;
        }
        if(direction.equals(Direction.EAST)){
            return Direction.WEST;
        }
        if(direction.equals(Direction.WEST)){
            return Direction.EAST;
        }
        throw new RuntimeException("getOppositeDirection: Invalid direction passed: " + direction);
    }

    public static Direction getInvertedDirection_NorthOrWest(Direction direction){
        switch (direction){
            case EAST:
            case WEST:
                return Direction.NORTH;
            case NORTH:
            case SOUTH:
            default:
                return Direction.WEST;
        }
    }

    public static boolean isReverseDirection(Direction direction){
        return direction == Direction.NORTH || direction == Direction.WEST;
    }



    public static Character getCharacterAtXY(ArrayList<String> layout, int[] xy){
        if(xy == null){
            return null;
        }
        int x = xy[0];
        int y = xy[1];
        if(y >= layout.size()){
            return null;
        }
        String row = layout.get(y);
        if(x >= row.length()){
            return null;
        }
        return row.charAt(x);
    }

    public static Character getAdjacentCharacter(ArrayList<String> layout, int[] xySourceChar, Direction direction){
        if(xySourceChar == null){
            return null;
        }
        int x = xySourceChar[0];
        int y = xySourceChar[1];
        try {
            if(x == 0 && direction.equals(Direction.WEST) ||
                    x == (layout.get(0).length() - 1) && direction.equals(Direction.EAST) ||
                    y == 0 && direction.equals(Direction.NORTH) ||
                    y == (layout.size() - 1) && direction.equals(Direction.SOUTH)){
                return null;
            }
            switch (direction){
                case NORTH:
                    return layout.get(y - 1).charAt(x);
                case SOUTH:
                    return layout.get(y + 1).charAt(x);
                case WEST:
                    return layout.get(y).charAt(x - 1);
                case EAST:
                    return layout.get(y).charAt(x + 1);
            }
        }catch (IndexOutOfBoundsException e){
            Log.e("getAdjacentCharacter", "Expected - Could not get character " + direction.name() + " of position " + x + ", " + y + ":\n" + e.getMessage());
            return null;
        }catch (Exception e){
            Log.e("getAdjacentCharacter", "Unexpected - Could not get character " + direction.name() + " of position " + x + ", " + y + ":\n" + e.getMessage());
            return null;
        }
        Log.e("getAdjacentCharacter", "Should not have reached here");
        return null;
    }

    public static int[] getAdjacentTileXY(ArrayList<String> layout, int[] xySourceChar, Direction direction){
        if(direction.equals(Direction.WEST)){
            if(getAdjacentCharacter(layout, xySourceChar, direction) != null){
                return new int[]{xySourceChar[0] - 1, xySourceChar[1]};
            }
            return null;
        }else if(direction.equals(Direction.EAST)){
            if(getAdjacentCharacter(layout, xySourceChar, direction) != null){
                return new int[]{xySourceChar[0] + 1, xySourceChar[1]};
            }
            return null;
        }else if(direction.equals(Direction.NORTH)){
            if(getAdjacentCharacter(layout, xySourceChar, direction) != null){
                return new int[]{xySourceChar[0], xySourceChar[1] - 1};
            }
            return null;
        }else if(direction.equals(Direction.SOUTH)){
            if(getAdjacentCharacter(layout, xySourceChar, direction) != null){
                return new int[]{xySourceChar[0], xySourceChar[1] + 1};
            }
            return null;
        }
        return null;
    }



    /**
     * @return null if there is no playable tile in this direction, else the first playable map tile in this direction
     */
    public static int[] getNextPlayableMapTileXY(ArrayList<String> layout, int[] sourceXY, Direction direction) {
        while (true){
            sourceXY = getAdjacentTileXY(layout, sourceXY, direction);
            if(sourceXY == null){
                return null;
            }
            Character c = getCharacterAtXY(layout, sourceXY);
            if(c == null || TileCodes.getBlockTileCodes().contains(c)){
                return null;
            }
            if(TileCodes.getPlayableTileCodes().contains(c)){
                return sourceXY;
            }
        }
    }

    public static Direction getDirection(int sourceX, int sourceY, int targetX, int targetY){
        if(sourceX < targetX){
            return Direction.EAST;
        }
        if(sourceX > targetX){
            return Direction.WEST;
        }
        if(sourceY < targetY){
            return Direction.SOUTH;
        }
        if(sourceY > targetY){
            return Direction.NORTH;
        }
        return null;
    }

    /**
     * Gets the xy's of all the possible playable tiles the player can use.
     * Filters out any playable tiles not accessible due to walls/non-playable etc. tile boundaries
     */
    public static ArrayList<int[]> getXYsOfAvailablePlayableTilesToPlayer(ArrayList<String> layout, int[] sourceXY){
        ArrayList<int[]> output = new ArrayList<>();
        ArrayList<int[]> xyQueue = new ArrayList<>();      //A list of xys we have yet to evaluate
        ArrayList<int[]> xyEvaluated = new ArrayList<>();  //A list of xys we have already evaluated
        xyQueue.add(sourceXY);
        while(!xyQueue.isEmpty()){
            int[] xy = xyQueue.get(0);
            if(!Util.contains(xyEvaluated, xy)){
                //We haven't evaluated this tile yet
                Character xyCode = getCharacterAt(layout, xy);
                if(Util.isPlayableTile(xyCode) && !Util.contains(output, xy)){
                    output.add(xy);
                }
                ArrayList<int[]> touchingXys = getXYsOfAdjacentTiles(layout, xy);
                for(int[] touchingXy : touchingXys){
                    if(!isBlockingTile(getCharacterAt(layout, xy)) && !Util.contains(xyEvaluated, touchingXy)){
                        //Add it to queue if we have not already evaluated this xy
                        xyQueue.add(touchingXy);
                    }
                }
                xyEvaluated.add(xy);
            }
            xyQueue.remove(0);
        }
        return output;
    }

    public static Character getCharacterAt(ArrayList<String> layout, int[] sourceXY) {
        try {
            if (layout.size() >= sourceXY[1]) {
                String row = layout.get(sourceXY[1]);
                if (row.length() >= sourceXY[0]) {
                    return row.charAt(sourceXY[0]);
                }
            }
        }catch (IndexOutOfBoundsException oob){
            return null;
        }
        return null;
    }

    public static ArrayList<int[]> getXYsOfAdjacentTiles(ArrayList<String> layout, int[] sourceXY){
        ArrayList<int[]> l = new ArrayList<>();
        Character cE = getAdjacentCharacter(layout, sourceXY, Direction.EAST);
        Character cW = getAdjacentCharacter(layout, sourceXY, Direction.WEST);
        Character cN = getAdjacentCharacter(layout, sourceXY, Direction.NORTH);
        Character cS = getAdjacentCharacter(layout, sourceXY, Direction.SOUTH);
        if(cE != null){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.EAST));
        }
        if(cW != null){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.WEST));
        }
        if(cN != null){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.NORTH));
        }
        if(cS != null){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.SOUTH));
        }
        return l;
    }

    public static boolean isNearBoardLetter(ArrayList<String> layout, int[] sourceXY){
        for(Position p : Position.values()){
            Character c = getCharacterAt(layout, p.getXy(sourceXY));
            if(c != null && Character.isLetter(c)){
                return true;
            }
        }
        return false;
    }

    public static boolean isAdjacentToBoardLetter(ArrayList<String> layout, int[] sourceXY){
        Position[] ps = new Position[]{Position.TOP, Position.BOTTOM, Position.LEFT, Position.RIGHT};
        for(Position p : ps){
            Character c = getCharacterAt(layout, p.getXy(sourceXY));
            if(c != null && Character.isLetter(c)){
                return true;
            }
        }
        return false;
    }

    public static String getRowOrColumnAsString(boolean isRow, ArrayList<String> layout, int[] xy){
        if(isRow){
            return getRowAsString(layout, xy[1]);
        }else {
            return getColumnAsString(layout, xy[0]);
        }
    }

    public static String getRowAsString(ArrayList<String> layout, int y) {
        if(y >= layout.size()){
            return null;
        }
        return layout.get(y);
    }

    public static String getColumnAsString(ArrayList<String> layout, int x) {
        if(x >= layout.get(0).length()){
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for(String row : layout){
            sb.append(row.charAt(x));
        }
        return sb.toString();
    }

    //Returns a list of xy coordinates of letters that are adjacent to sourceXY tile
    public static ArrayList<int[]> getXYsOfAdjacentLetters(ArrayList<String> layout, int[] sourceXY){
        ArrayList<int[]> l = new ArrayList<>();
        Character cE = getAdjacentCharacter(layout, sourceXY, Direction.EAST);
        Character cW = getAdjacentCharacter(layout, sourceXY, Direction.WEST);
        Character cN = getAdjacentCharacter(layout, sourceXY, Direction.NORTH);
        Character cS = getAdjacentCharacter(layout, sourceXY, Direction.SOUTH);
        if(cE != null && Character.isLetter(cE)){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.EAST));
        }
        if(cW != null && Character.isLetter(cW)){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.WEST));
        }
        if(cN != null && Character.isLetter(cN)){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.NORTH));
        }
        if(cS != null && Character.isLetter(cS)){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.SOUTH));
        }
        return l;
    }

    public static ArrayList<int[]> getXYsOfTouchingPlayableTiles(ArrayList<String> layout, int[] sourceXY){
        ArrayList<int[]> l = new ArrayList<>();
        Character cE = getAdjacentCharacter(layout, sourceXY, Direction.EAST);
        Character cW = getAdjacentCharacter(layout, sourceXY, Direction.WEST);
        Character cN = getAdjacentCharacter(layout, sourceXY, Direction.NORTH);
        Character cS = getAdjacentCharacter(layout, sourceXY, Direction.SOUTH);
        if(cE != null && TileCodes.getPlayableTileCodes().contains(cE)){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.EAST));
        }
        if(cW != null && TileCodes.getPlayableTileCodes().contains(cW)){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.WEST));
        }
        if(cN != null && TileCodes.getPlayableTileCodes().contains(cN)){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.NORTH));
        }
        if(cS != null && TileCodes.getPlayableTileCodes().contains(cS)){
            l.add(getAdjacentTileXY(layout, sourceXY, Direction.SOUTH));
        }
        return l;
    }

    public static ArrayList<int[]> getXYsOfTouchingTilesOfTypeRecursively(ArrayList<String> layout, int[] sourceXY, TileCodes ofType){
        return getXYsOfTouchingTilesOfTypeRecursively(layout, sourceXY, ofType, null);
    }

    public static ArrayList<int[]> getXYsOfTouchingRogueTilesRecursively(ArrayList<String> layout, int[] sourceXY) {
        return getXYsOfTouchingTilesOfTypeRecursively(layout, sourceXY, null, true);
    }

    public static ArrayList<int[]> getXYsOfTouchingLetterTilesRecursively(ArrayList<String> layout, int[] sourceXY) {
        return getXYsOfTouchingTilesOfTypeRecursively(layout, sourceXY, null, false);
    }

    /**
     * Get a list of xy's of all touching tiles to source of type supplied.
     * Recursively continues in all directions until there is no more tiles of type supplied adjacent.
     * @param layout
     * @param sourceXY
     * @param ofType
     * @param overrideLetter1Rogue0Board null by default.
     *                                       True:  previous param ofType is ignored; returns touching Rogue Letter tiles recursively
     *                                       False: previous param ofType is ignored; returns touching Letter tiles recursively
     */
    public static ArrayList<int[]> getXYsOfTouchingTilesOfTypeRecursively(ArrayList<String> layout, int[] sourceXY, TileCodes ofType, Boolean overrideLetter1Rogue0Board){
        ArrayList<int[]> output = new ArrayList<>();
        ArrayList<int[]> xyQueue = new ArrayList<>();      //A list of xys we have yet to evaluate
        ArrayList<int[]> xyEvaluated = new ArrayList<>();  //A list of xys we have already evaluated
        xyQueue.add(sourceXY);
        while(!xyQueue.isEmpty()){
            int[] xy = xyQueue.get(0);
            if(!Util.contains(xyEvaluated, xy)){
                //We haven't evaluated this tile yet
                Character xyCode = getCharacterAt(layout, xy);
                boolean addToOutput;
                if(overrideLetter1Rogue0Board == null) {
                    addToOutput = xyCode.equals(ofType.getCharacter());
                }else {
                    if(overrideLetter1Rogue0Board){
                        addToOutput = isRogueTile(xyCode);
                    }else {
                        addToOutput = !Util.isBoardLetterTile(xyCode);
                    }
                }
                if(addToOutput && !Util.contains(output, xy)){
                    output.add(xy);
                }
                ArrayList<int[]> touchingXys = getXYsOfAdjacentTiles(layout, xy);
                for(int[] touchingXy : touchingXys){
                    Character c = getCharacterAt(layout, touchingXy);
                    boolean addToQueue;
                    if(overrideLetter1Rogue0Board == null) {
                        addToQueue = c.equals(ofType.getCharacter());
                    }else {
                        if(overrideLetter1Rogue0Board){
                            addToQueue = Util.isRogueTile(c);
                        }else {
                            addToQueue = !Util.isBoardLetterTile(c);
                        }
                    }
                    if(addToQueue && !Util.contains(xyEvaluated, touchingXy)){
                        //Add it to queue if we have not already evaluated this xy
                        xyQueue.add(touchingXy);
                    }
                }
                xyEvaluated.add(xy);
            }
            xyQueue.remove(0);
        }
        return output;
    }

    public static ArrayList<int[]> getLetterXYs(ArrayList<HintWord> words) {
        ArrayList<int[]> output = new ArrayList<>();
        for(HintWord w : words){
            ArrayList<int[]> xys = getLetterXYs(w);
            for(int[] xy : xys){
                if(!Util.contains(output, xy)){
                    output.add(xy);
                }
            }
        }
        return output;
    }

    public static ArrayList<int[]> getLetterXYs(HintWord playedWord) {
        ArrayList<int[]> output = new ArrayList<>();
        for(HintLetter l : playedWord.getLetters()){
            output.add(l.getXY());
        }
        return output;
    }

    public static boolean isRogueTile(Character c) {
        return Character.isLetter(c) && Character.isUpperCase(c);
    }

    public static boolean isBoardLetterTile(Character c) {
        return Character.isLetter(c) && Character.isLowerCase(c);
    }

    public static boolean isPlayableTile(Character tile) {
        return tile != null && TileCodes.getPlayableTileCodes().contains(tile);
    }

    public static boolean isBlockingTile(Character tile) {
        return tile != null && TileCodes.getBlockTileCodes().contains(tile);
    }

    public static boolean equal(Integer[] intArray1, Integer[] intArray2) {
        if(intArray1 == null && intArray2 == null){
            return true;
        }
        if(intArray1 == null || intArray2 == null){
            return false;
        }
        if(intArray1.length != intArray2.length){
            return false;
        }
        for(int i = 0; i < intArray1.length; i++){
            if(intArray1[i] != intArray2[i]){
                return false;
            }
        }
        return true;
    }

    public static boolean equal(int[] intArray1, int[] intArray2) {
        if(intArray1 == null && intArray2 == null){
            return true;
        }
        if(intArray1 == null || intArray2 == null){
            return false;
        }
        if(intArray1.length != intArray2.length){
            return false;
        }
        for(int i = 0; i < intArray1.length; i++){
            if(intArray1[i] != intArray2[i]){
                return false;
            }
        }
        return true;
    }

    public static boolean contains(final ArrayList<int[]> parent, final int[] child){
        for(final int[] item : parent){
            if(Arrays.equals(item, child)){
                return true;
            }
        }
        return false;
    }

    public static Integer indexOf(int[] target, ArrayList<int[]> array) {
        for(int i = 0; i < array.size(); i++){
            if(array.get(i)[0] == target[0] && array.get(i)[1] == target[1]){
                return i;
            }
        }
        return null;
    }

    public static ArrayList<String> applyHintWordToLevel(ArrayList<String> layout, HintWord answer) {
        for (HintLetter l : answer.getLetters()){
            String oldRow = layout.get(l.getRow());
            String newRow = oldRow.substring(0, l.getColumn()) + l.getLetter().toString().toUpperCase() + oldRow.substring(l.getColumn()+1);
            layout.set(l.getRow(), newRow);
        }
        return layout;
    }

    public static String getLevelAsString(ArrayList<String> layout){
        String s = "";
        for(String l : layout){
            s += l + "\n";
        }
        return s;
    }

    /**
     * Given the players current xy, returns the xy the player should be aiming to reach next.
     * Normally the ENDZONE is the target. Make sure we are aiming for the endzone on our side.
     * Sometimes we may have other targets first as well e.g. detonator, points etc.
     * As a result, the result of this method may be different each turn
     */
    public static int[] getOptimalTargetXY(ArrayList<String> layout, int[] sourceXY){
        //Target priority:
        //#1: Detonator
        //#2. Endzone
        //#3. No target (i.e. aiming to reach high points to open a gate) = NULL
        ArrayList<int[]> availableXys = getXYsOfAvailablePlayableTilesToPlayer(layout, sourceXY);
        int[] optimalXY = null;
        ArrayList<int[]> endZoneXYs = new ArrayList<>();
        for (int[] xy : availableXys){
            Character tilecode = getCharacterAt(layout, xy);
            if(TileCodes.Detonator.getCharacter() == tilecode){
                optimalXY = xy;
                break;
            }
            if(TileCodes.EndZone.getCharacter() == tilecode){
                optimalXY = xy;
                endZoneXYs.add(xy);
            }
        }
        Integer bestScore = null;
        for (int[] endXy : endZoneXYs){
            int score = AStar.getPathScore(new AStar(layout, sourceXY, endXy).findPath());
            if(bestScore == null || score < bestScore){
                bestScore = score;
                optimalXY = endXy;
            }
        }
        return optimalXY;
    }

    public static double getFactor(double mostFreq) {
        int numDigits = String.valueOf(mostFreq).length();
        String f = "1";
        for(int i = 1; i < numDigits; i++){
            f += "0";
        }
        f += ".0";
        return Double.valueOf(f);
    }

    public static String getBaseWord(String word) {
        if(!SpecialCharacters.containsSpecialCharacters(word)){
            return word;
        }
        String baseWord = "";
        for(Character c : word.toCharArray()){
            baseWord += SpecialCharacters.getBaseCharacter(c);
        }
        return baseWord;
    }

    public static HintWord getInitialBoardWord(DatabaseGateway databaseGateway, int[] playerStartingXY, ArrayList<String> level){
        ArrayList<int[]> unorderedLettersXY = getLetterTilesTouchingLetter(level, playerStartingXY);
        if(Util.contains(unorderedLettersXY, playerStartingXY) && !Character.isLetter(getCharacterAt(level, playerStartingXY))){
            unorderedLettersXY.remove(playerStartingXY);
        }
        return getWordFromUnorderedLetters(databaseGateway, level, unorderedLettersXY);
    }

    public static ArrayList<int[]> getLetterTilesTouchingLetter(ArrayList<String> layout, int[] letterTileXY){
        ArrayList<int[]> result = new ArrayList<>();
        result.add(letterTileXY);
        return getLetterTilesTouchingLetter(layout, letterTileXY, result);
    }

    public static ArrayList<int[]> getLetterTilesTouchingLetter(ArrayList<String> layout, int[] letterTileXY, ArrayList<int[]> listSoFar){
        ArrayList<int[]> xYofTouchingLetters = getXYsOfAdjacentLetters(layout, letterTileXY);
        for (int[] l : xYofTouchingLetters){
            if(!Util.contains(listSoFar, l)){
                listSoFar.add(l);
                getLetterTilesTouchingLetter(layout, l, listSoFar);
            }
        }
        return listSoFar;
    }

    public static HintWord getWordFromUnorderedLetters(DatabaseGateway databaseGateway, ArrayList<String> layout, ArrayList<int[]> unorderedLettersXY) {
        if(unorderedLettersXY.isEmpty()){
            return null;
        }
        if(unorderedLettersXY.size() == 1){
            return new HintWord(new HintLetter(unorderedLettersXY.get(0)[0], unorderedLettersXY.get(0)[1],
                    getCharacterAtXY(layout, unorderedLettersXY.get(0)), true, false));
        }
        boolean isHorizontal = unorderedLettersXY.get(0)[1] == unorderedLettersXY.get(1)[1];
        int[] startingXY = unorderedLettersXY.get(0);
        while (true){
            Character c = getAdjacentCharacter(layout, startingXY, isHorizontal ? Direction.WEST : Direction.NORTH);
            if(c != null && Character.isLetter(c)){
                startingXY = new int[]{startingXY[0] - (isHorizontal ? 1 : 0) , startingXY[1] - (isHorizontal ? 0 : 1)};
            }else {
                break;
            }
        }
        ArrayList<HintLetter> letters = new ArrayList<>();
        letters.add(new HintLetter(startingXY[0], startingXY[1], getCharacterAtXY(layout, startingXY), true, false));
        while (true){
            startingXY = new int[]{startingXY[0] + (isHorizontal ? 1 : 0) , startingXY[1] + (isHorizontal ? 0 : 1)};
            Character c = getCharacterAtXY(layout, startingXY);
            if(c != null && Character.isLetter(c)){
                letters.add(new HintLetter(startingXY[0], startingXY[1],
                        getCharacterAtXY(layout, startingXY), true, false));
            }else {
                break;
            }
        }
        HintWord word = new HintWord(letters);
        if(databaseGateway.isWordExists(word.toString())){
            return new HintWord(word, databaseGateway);
        }else {
            String wordAsString = new StringBuilder(word.toString()).reverse().toString();
            if(databaseGateway.isWordExists(wordAsString)){
                HintWord reversedWord = new HintWord(new ArrayList<HintLetter>());
                for(HintLetter l : word.getLetters()){
                    reversedWord.addLetter(l, true);
                }
                return word;
            }else {
                throw new RuntimeException(String.format("There is a word [%s] on the board that is not in the dictionary!\n%s",
                        wordAsString, getLevelAsString(layout)));
            }
        }
    }

    public static int[] getFirstXYof(ArrayList<String> layout, TileCodes tileCode){
        return getFirstXYof(layout, tileCode.getCharacter());
    }

    public static int[] getFirstXYof(ArrayList<String> layout, Character target){
        int x = 0;
        int y = 0;
        for (String s : layout){
            for(Character c : s.toCharArray()){
                if(c.equals(target)){
                    return new int[]{x, y};
                }
                x++;
            }
            x = 0;
            y++;
        }
        return null;
    }

    public static final String join(final char pSeparator, final Object ... pStrings) {
        final StringBuilder stringBuilder = new StringBuilder();

        final int count = pStrings.length;
        for (int i = 0; i < count; i++) {
            stringBuilder.append(pStrings[i]);
            if (i < count - 1) {
                stringBuilder.append(pSeparator);
            }
        }

        return stringBuilder.toString();
    }

    public static final String join(final String pSeparator, final Object ... pStrings) {
        final StringBuilder stringBuilder = new StringBuilder();

        final int count = pStrings.length;
        for (int i = 0; i < count; i++) {
            stringBuilder.append(pStrings[i]);
            if (i < count - 1) {
                stringBuilder.append(pSeparator);
            }
        }

        return stringBuilder.toString();
    }

    public static int countMatches(String line, Character character) {
        return line.length() - line.replace(character.toString(), "").length();
    }


    public static int getConsonantsCount(String hudLetters) {
        int ret = 0;
        ArrayList<Character> consonants = new ArrayList<Character>();
        Arrays.asList(getConsonants());
        for(Character c : hudLetters.toCharArray()){
            if(consonants.contains(c)){
                ret++;
            }
        }
        return ret;
    }

    public static int getVowelsCount(String hudLetters) {
        int ret = 0;
        ArrayList<Character> vowels = new ArrayList<Character>();
        Arrays.asList(getVowels());
        for(Character c : hudLetters.toCharArray()){
            if(vowels.contains(c)){
                ret++;
            }
        }
        return ret;
    }

    public static Character[] getVowels(){
        return new Character[]{'a', 'e', 'i', 'o', 'u'};
    }

    public static Character[] getConsonants(){
        return new Character[]{'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};
    }

    public static int[] isWordOnEndzone(ArrayList<String> layoutOriginal, HintWord answer){
        return isWordOnTileCode(layoutOriginal, answer, TileCodes.EndZone);
    }

    public static int[] isWordOnDetonator(ArrayList<String> layoutOriginal, HintWord answer){
        return isWordOnTileCode(layoutOriginal, answer, TileCodes.Detonator);
    }

    /**
     * @return the x,y of the letter that has been placed on the specified tileCode.
     *         null if the tileCode is not covered by any of the answer's letters.
     */
    public static int[] isWordOnTileCode(ArrayList<String> layoutOriginal, HintWord answer, TileCodes tileCode) {
        if(answer == null){
            return null;
        }
        for(HintLetter l : answer.getLetters()){
            Character originalChar = getCharacterAt(layoutOriginal, l.getXY());
            if(originalChar != null && tileCode.getCharacter().equals(originalChar)){
                return l.getXY();
            }
        }
        return null;
    }

    /**
     * Updates the layout modified data if allowed
     * @param xy where the new character will be written
     * @param updatedCode new character to write
     * @param uniqueCode means there can only be 1 instance of this tile on the board.
     *                   If true, revert any other instances of this tile with the underlying tile code
     */
    public static void updateLayout(ArrayList<String> layoutOriginal, ArrayList<String> layoutModified, int[] xy, TileCodes updatedCode, boolean uniqueCode){
        if(xy == null || xy.length != 2 || updatedCode == null){
            Log.e(Util.class.getSimpleName(), "Validation failed!");
            return;
        }
        if(updatedCode.equals(TileCodes.Invisible)){
            //Invisible codes don't affect the layout
            return;
        }
        String before = layoutModified.get(xy[1]);
        if(before.charAt(xy[0]) == TileCodes.WallTile.getCharacter() || Character.isLetter(before.charAt(xy[0]))){
            Log.e(Util.class.getSimpleName(), "cannot overwrite Wall [-] or Letter");
            return;
        }
        char[] beforeChars = before.toCharArray();
        beforeChars[xy[0]] = updatedCode.getCharacter();
        layoutModified.set(xy[1], String.valueOf(beforeChars));
        if(uniqueCode){
            enforceCodeExistsOnlyAtXY(layoutOriginal, layoutModified, updatedCode, xy, uniqueCode);
        }
    }

    public static void updateLayout(ArrayList<String> layoutOriginal, ArrayList<String> layoutModified, int[] xy, Character updatedCode){
        if(xy == null || xy.length != 2 || updatedCode == null){
            Log.e(Util.class.getSimpleName(), "Validation failed!");
            return;
        }
        String before = layoutModified.get(xy[1]);
        if(before.charAt(xy[0]) == TileCodes.WallTile.getCharacter() || Character.isLetter(before.charAt(xy[0]))){
            Log.e(Util.class.getSimpleName(), "cannot overwrite Wall [-] or Letter");
            return;
        }
        char[] beforeChars = before.toCharArray();
        beforeChars[xy[0]] = updatedCode;
        layoutModified.set(xy[1], String.valueOf(beforeChars));
    }

    private static void enforceCodeExistsOnlyAtXY(ArrayList<String> layoutOriginal, ArrayList<String> layoutModified, TileCodes updatedCode, int[] xy, boolean uniqueCode){
        for(int r = 0; r < layoutModified.size(); r++){
            String row = layoutModified.get(r);
            for(int c = 0; c < row.length(); c++){
                Character character = row.charAt(c);
                if(character == updatedCode.getCharacter()){
                    //An instance of our updated character. If this is not the specified xy, revert it
                    int[] _xy = new int[]{c, r};
                    if(!Util.equal(xy, _xy)) {
                        revertLayoutModified(layoutOriginal, layoutModified, xy, uniqueCode);
                    }
                }
            }
        }
    }

    /**
     * Revert the code at xy in layout modified (that is, make it equal to the corresponding layout original value).
     * NOTE: StartingZone is reverted to a map tile
     * @param xy
     * @param uniqueCode
     */
    public static void revertLayoutModified(ArrayList<String> layoutOriginal, ArrayList<String> layoutModified, int[] xy, boolean uniqueCode){
        Character original = getCharacterAt(layoutOriginal, xy).charValue();
        /**
         * There are some exceptions when reverting to the original layout character.
         * Depending on events in the game we may not want the initial original value...
         */
        ArrayList<Character> exceptions = new ArrayList<>();
        //Players starting zone is a map tile. So when they focus somewhere else, the original starting zone is the same as any other map tile.
        exceptions.addAll(TileCodes.getPlayerStartingZoneTileCodes());
        //Gates, detonator crates are essentially map tiles. Once the player can focus on them, that means they are gone.
        exceptions.addAll(TileCodes.getBlockTileCodes());
        if(exceptions.contains(original)){
            original = TileCodes.MapTile.getCharacter();
        }
        updateLayout(layoutOriginal, layoutModified, xy, TileCodes.getTileCode(original), uniqueCode);
    }

    /**
     * Given a letter on the board, return all words that use that specific letter (either locked or temporary)
     */
    public static ArrayList<HintWord> getWordsOfBoardLetter(DatabaseGateway databaseGateway,
                                                            final ArrayList<String> layout,
                                                            final int[] xyBoardLetter,
                                                            final HintWord answerPlayedThisTurn){
        final boolean yes = true;
        ArrayList<HintWord> output = new ArrayList<>();
        ArrayList<HintLetter> word1Letters = new ArrayList<HintLetter>(){
            {addAll(getAdjoiningLettersInDirection(layout, xyBoardLetter, Direction.EAST, answerPlayedThisTurn, yes, yes));}
            {addAll(getAdjoiningLettersInDirection(layout, xyBoardLetter, Direction.WEST, answerPlayedThisTurn, yes, yes));}
        };
        ArrayList<HintLetter> word2Letters = new ArrayList<HintLetter>(){
            {addAll(getAdjoiningLettersInDirection(layout, xyBoardLetter, Direction.NORTH, answerPlayedThisTurn, yes, yes));}
            {addAll(getAdjoiningLettersInDirection(layout, xyBoardLetter, Direction.SOUTH, answerPlayedThisTurn, yes, yes));}
        };
        if(!word1Letters.isEmpty()){
            word1Letters.add(new HintLetter(xyBoardLetter, getCharacterAtXY(layout, xyBoardLetter), yes, false));
            output.add(Util.getWordFromUnorderedLetters(databaseGateway, layout, Util.getLetterXYs(new HintWord(word1Letters))));
        }
        if(!word2Letters.isEmpty()){
            word2Letters.add(new HintLetter(xyBoardLetter, getCharacterAtXY(layout, xyBoardLetter), yes, false));
            output.add(Util.getWordFromUnorderedLetters(databaseGateway, layout, Util.getLetterXYs(new HintWord(word2Letters))));
        }
        return output;
    }

    /**
     * Given a board letter xy, get any board letters touching it in a specific direction (starting with the nearest | either locked or temporary)
     */
    public static ArrayList<HintLetter> getAdjoiningLettersInDirection(ArrayList<String> layout,
                                                                       int[] xyBoardLetter,
                                                                       Direction direction,
                                                                       HintWord answerPlayedThisTurn,
                                                                       boolean includeBoardLetterTiles,
                                                                       boolean includeRogueLetterTiles){
        ArrayList<HintLetter> output = new ArrayList<>();
        int[] nextXy = getAdjacentTileXY(layout, xyBoardLetter, direction);
        Character nextC = getCharacterAtXY(layout, nextXy);
        if(nextC == null){
            return output;
        }
        while (nextC != null){
            HintLetter l = null;
            for(HintLetter aL : answerPlayedThisTurn.getLetters()){
                if(Util.equal(aL.getXY(), nextXy)){
                    l = new HintLetter(aL);
                    break;
                }
            }
            if(l == null) {
                //If letter at nextXy isn't part of answerPlayedThisTurn, means its locked on the board
                l = new HintLetter(nextXy, nextC, true, false);
            }
            if((includeBoardLetterTiles && Util.isBoardLetterTile(nextC)) ||
                    (includeRogueLetterTiles && Util.isRogueTile(nextC))){
                output.add(l);
            }else {
                break;
            }
            nextXy = getAdjacentTileXY(layout, nextXy, direction);
            nextC = getCharacterAtXY(layout, nextXy);
        }
        return output;
    }

    private static final int LENGTH_BLOCK_LINE = 76;

    public static String getBlockWithText(Character blockSymbol, String text){
        return getBlockWithText(blockSymbol, text, false);
    }

    public static String getBlockWithText(Character blockSymbol, String text, boolean isDoubleLineBorder){
        int spaces = LENGTH_BLOCK_LINE - text.length() - 4;
        int spacesL;
        int spacesR;
        if(spaces % 2 == 0){
            spacesL = spacesR = (spaces / 2);
        }else {
            spacesL = ((spaces - 1) / 2);
            spacesR = ((spaces + 1) / 2);
        }
        String fullSymbolLine = new String(new char[LENGTH_BLOCK_LINE]).replace('\0', blockSymbol);
        String doubleSymbol = blockSymbol + "" + blockSymbol;
        StringBuilder output = new StringBuilder();
        output.append(fullSymbolLine + "\n");
        if(isDoubleLineBorder){
            output.append(fullSymbolLine + "\n");
        }
        output.append(doubleSymbol + new String(new char[spacesL]).replace('\0', ' ') + text
                + new String(new char[spacesR]).replace('\0', ' ') + doubleSymbol);
        output.append("\n" + fullSymbolLine);
        if(isDoubleLineBorder){
            output.append("\n" + fullSymbolLine);
        }
        return output.toString();
    }


}

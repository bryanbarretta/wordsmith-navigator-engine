package bryans.work.wordsmithnavigatorengine.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Configuration {

    private static final String FILE_CONFIG = "config.txt";
    private HashMap<String, String> configs;

    private static Configuration instance = null;

    // static method to create instance of Singleton class
    public static Configuration getInstance(Context context)
    {
        if (instance == null){
            try {
                instance = new Configuration(context);
            } catch (IOException e) {
                throw new RuntimeException("Fatal Error: Unable to load config.txt\n" + e.getMessage());
            }
        }

        return instance;
    }

    private Configuration(Context context) throws IOException {
        setup(context);
    }

    private void setup(Context context) throws IOException {
        configs = new HashMap<>();
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(FILE_CONFIG);
        String line;
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( inputStream ) );
        while((line = bufferedReader.readLine()) != null ) {
            line = line.trim();
            if (line.isEmpty() || !line.contains("=")) {
                continue;
            }
            String key = line.substring(0, line.indexOf('='));
            String value = line.substring(line.indexOf('=') + 1);
            configs.put(key, value);
        }
    }

    public String getStringValue(String key){
        return getStringValue(key, null);
    }

    public String getStringValue(String key, String defaultValue){
        if(!configs.containsKey(key)){
            return defaultValue;
        }
        return configs.get(key);
    }

}

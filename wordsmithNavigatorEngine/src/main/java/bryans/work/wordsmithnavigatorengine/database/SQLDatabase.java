package bryans.work.wordsmithnavigatorengine.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Bryan on 24/07/2016.
 */
public class SQLDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "wordnavenginedb";
    public static final int DATABASE_VERSION = 1;  //Increment this when schema changes
    private Context mContext;

    public SQLDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TableDictionary.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableCPULogicLogs.TABLE_NAME);

        onCreate(db);
    }

    private void createTables(SQLiteDatabase db){

        db.execSQL("create table " + TableDictionary.TABLE_NAME + " (" +
                TableDictionary.COL_WORD + " TEXT PRIMARY KEY," +
                TableDictionary.COL_DISPLAY_WORD + " TEXT," +
                TableDictionary.COL_FREQUENCY + " DOUBLE);");

        db.execSQL("create table " + TableCPULogicLogs.TABLE_NAME + " (" +
                TableCPULogicLogs.COL_GAME_NAME + " TEXT," +
                TableCPULogicLogs.COL_TURN + " INTEGER," +
                TableCPULogicLogs.COL_LOG_TYPE + " TEXT," +
                TableCPULogicLogs.COL_VALUE + " TEXT," + "" +
                "PRIMARY KEY ("
                    + TableCPULogicLogs.COL_GAME_NAME + ", "
                    + TableCPULogicLogs.COL_TURN + ", "
                    + TableCPULogicLogs.COL_LOG_TYPE + "));");

    }

}

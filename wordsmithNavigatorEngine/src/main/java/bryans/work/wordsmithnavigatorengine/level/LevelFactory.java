package bryans.work.wordsmithnavigatorengine.level;

import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import bryans.work.wordsmithnavigatorengine.enums.SupportedLanguages;

/**
 * Loads all the levels static and player data
 * Created by Bryan on 19/08/2018.
 */

public class LevelFactory {

    private HashMap<Integer, LevelStaticData> mapIdLevelStaticData;
    private HashMap<Integer, LevelPlayerData> mapIdLevelPlayerData;
    private SupportedLanguages lang;

    public LevelFactory(AssetManager assets, SupportedLanguages lang) {
        this.lang = lang;
        try {
            loadStaticLevelData(assets);
            mapIdLevelPlayerData = new HashMap<>();
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    //region get level objects

    public LevelStaticData getLevelStaticData(int id){
        return mapIdLevelStaticData.get(id);
    }

    public LevelPlayerData getLevelPlayerData(int id){
        if(!mapIdLevelPlayerData.containsKey(id)){
            mapIdLevelPlayerData.put(id, new LevelPlayerData(mapIdLevelStaticData.get(id)));
        }
        return mapIdLevelPlayerData.get(id);
    }

    public ArrayList<Level> getLevels(int from, int to) {
        ArrayList<Level> levels = new ArrayList<>();
        for(int i = from; i < to; i++){
            levels.add(new Level(getLevelStaticData(i), getLevelPlayerData(i)));
        }
        return levels;
    }

    //endregion

    //region get info about level progress

    public int getLatestUnlockedLevelId(){
        return mapIdLevelPlayerData.size();
    }

    public int getTotalUnlockedStarCount(){
        int res = 0;
        for(int i = 1; i <= getLatestUnlockedLevelId(); i++){
            for(Boolean u : mapIdLevelPlayerData.get(i).getMapIdStarUnlocked().values()){
                res += (u != null && u == true) ? 1 : 0;
            }
        }
        return res;
    }

    public int getTotalLevelCount() {
        return mapIdLevelStaticData.size();
    }

    //endregion

    private void loadStaticLevelData(AssetManager assets) throws IOException {
        mapIdLevelStaticData = new HashMap<>();
        ArrayList<String> arrLevel = new ArrayList<>();
        InputStream inputStream = assets.open("levels_" + lang.getAbbreviation().toLowerCase() + ".txt");
        String line;
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( inputStream ) );
        while((line = bufferedReader.readLine()) != null )
        {
            line = line.trim();
            if(line.isEmpty()){
                continue;
            }
            else if(line.startsWith("#")){
                continue;
            }
            else {
                if(line.startsWith("[")){
                    String key = line.substring(1, line.trim().indexOf("]"));
                    if(android.text.TextUtils.isDigitsOnly(key) && !arrLevel.isEmpty()){
                        //Assign current level to list, reset and start new level
                        LevelStaticData levelStaticData = LevelStaticData.newInstance(arrLevel);
                        mapIdLevelStaticData.put(levelStaticData.getId(), levelStaticData);
                        arrLevel = new ArrayList<>();
                    }
                }
                arrLevel.add(line);
            }
        }
        if(!arrLevel.isEmpty()){
            LevelStaticData levelStaticData = LevelStaticData.newInstance(arrLevel);
            mapIdLevelStaticData.put(levelStaticData.getId(), levelStaticData);
        }
    }

    //endregion

}

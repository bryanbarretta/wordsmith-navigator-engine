package bryans.work.wordsmithnavigatorengine.ai;

import android.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import bryans.work.wordsmithnavigatorengine.astar.AStar;
import bryans.work.wordsmithnavigatorengine.astar.Node;
import bryans.work.wordsmithnavigatorengine.asynctask.AsyncTaskHandler;
import bryans.work.wordsmithnavigatorengine.asynctask.iTask;
import bryans.work.wordsmithnavigatorengine.database.DatabaseGateway;
import bryans.work.wordsmithnavigatorengine.enums.Difficulty;
import bryans.work.wordsmithnavigatorengine.enums.Direction;
import bryans.work.wordsmithnavigatorengine.enums.TileCodes;
import bryans.work.wordsmithnavigatorengine.logs.CpuLogger;
import bryans.work.wordsmithnavigatorengine.logs.CpuLoggerInterface;
import bryans.work.wordsmithnavigatorengine.objects.HintLetter;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.objects.PathFinderOption;
import bryans.work.wordsmithnavigatorengine.util.Util;

/**
 * The CPUWordEngineAi is responsible for determining what word the CPU will play on its next turn.
 * The word it plays will be based on difficulty and optimal path to the endzone (higher error ratio on lower difficulties).
 * The CPUWordEngineAi will begin working as soon as possible, and should be running in the background while other players are going.
 *
 * The logic workflow is as follows:
 * 1. Calculate departure keys for next turn. These originate from all available map tiles around my letters played in my last turn.
 * 2. Using AStar, calculate a path score for each departure key.
 * |
 * 3. Choose a path (Best available path score [not already evaluated] on highest difficulty, or offset to lower difficulty accordingly)
 * 4. Get valid legal words for that path (Longest and most CPU intensive step, especially going WEST and NORTH)
 * 5. Is it my turn yet ?
 * |  - Yes: If I have at least 1 path option, Go to step 6. Otherwise back to step 3.
 * |  - No:  Go to step 3
 * |
 * 6. Select a word from one of my path options relevant to the difficulty
 */
public abstract class CPUWordEngineAi {

    private CpuLoggerInterface cpuLogger;
    private ArrayList<PathFinderOption> evaluatedPathFinderOptions = new ArrayList<>();
    private DatabaseGateway databaseGateway;
    private final Long seed;

    public CPUWordEngineAi(DatabaseGateway databaseGateway, Long seed, CpuLoggerInterface cpuLogger){
        this.databaseGateway = databaseGateway;
        this.seed = seed;
        this.cpuLogger = cpuLogger;
    }

    public abstract void onWordReady(HintWord word);

    /**
     * Starts finding a word for the cpu. When a word is found, abstract method onWordFound is called
     * @param asyncTaskHandler Provide this as this logic is carried out asynchronously
     * @param playedWords the list of played hintword objects already on the board for this user
     * @param layout map
     * @param targetXY endzone/detonator etc.
     * @param hudLetters players hud letters
     * @param difficulty AI
     * @param minFrequencyWord
     */
    public void findNextWord(AsyncTaskHandler asyncTaskHandler,
                             final ArrayList<HintWord> playedWords,
                             final ArrayList<String> layout,
                             final int[] targetXY,
                             final String hudLetters,
                             final Difficulty difficulty,
                             final double minFrequencyWord){
        asyncTaskHandler.AddTask(new iTask() {
            @Override
            public void Task() {
                cpuLogger.logLayout();
                cpuLogger.logPlayedWords(playedWords);
                cpuLogger.logTargetXy(targetXY);
                evaluatedPathFinderOptions = new ArrayList<>();
                try {
                    /*1*/
                    ArrayList<String> departureKeys = step1_calculateDepartureKeys(Util.getLetterXYs(playedWords), layout);
                    cpuLogger.logDepartureKeys(departureKeys);
                    /*2*/
                    ArrayList<PathFinderOption> pathFinderOptions;
                    if(targetXY != null) {
                        pathFinderOptions = step2_executeAStar(departureKeys, layout, targetXY, difficulty);
                    }else {
                        //No targetXY, which means the cpu is trying to gather points to open a gate
                        //In this case, every playable tile is a target
                        pathFinderOptions = step2_executeAStarWithoutTarget(departureKeys, layout, difficulty);
                    }

                    HintWord answer = null;
                    StringBuilder pfoLogs = new StringBuilder();
                    HintManager hm = null;
                    while (answer == null) {
                        pathFinderOptions.removeAll(evaluatedPathFinderOptions); //First time does nothing, but from then on stops re-evaluating same path
                        if(pathFinderOptions.isEmpty()){
                            break; //give up
                        }

                        /*3*/
                        PathFinderOption pathFinderOption = step3_choosePath(pathFinderOptions,
                                targetXY == null ? false : Util.getCharacterAtXY(layout, targetXY).equals(TileCodes.EndZone.getCharacter()),
                                difficulty);
                        /*4*/
                        hm = step4_populatePathWithAnswers(pathFinderOption, layout, hudLetters, minFrequencyWord);
                        pfoLogs.append(String.format("Departure Key %s of %s: %s = %s possible words",
                                evaluatedPathFinderOptions.size(),
                                evaluatedPathFinderOptions.size() + pathFinderOptions.size(),
                                getDepartureKeyEnglish(pathFinderOption.getXy(), pathFinderOption.getDirection()),
                                pathFinderOption.getWords().size()));

                        evaluatedPathFinderOptions.add(pathFinderOption);

                        /*5*/
                        if(!pathFinderOption.getWords().isEmpty()) {
                            answer = step5_selectAnswer(evaluatedPathFinderOptions.get(evaluatedPathFinderOptions.size() - 1), layout, difficulty);
                            if(answer != null) {
                                cpuLogger.logAStarData(pathFinderOptions);
                            }
                        }
                    }
                    cpuLogger.logPathFinderOptionEvaluation(pfoLogs.toString());
                    if(answer == null){
                        pfoLogs.append(" (0 after applying filters)");
                    }else {
                        if(hm != null){
                            logAnswerDecisionLogic(hm, answer);
                        }else {
                            cpuLogger.logAnswer(answer);
                        }
                    }
                    onWordReady(answer);
                }catch (Exception e){
                    throw new RuntimeException("Error in CPUWordEngine: " + e.getMessage());
                }
            }
            @Override
            public void OnTaskCompleted() {

            }
        });
    }

    private void logAnswerDecisionLogic(HintManager hm, HintWord answer) {
        if(!cpuLogger.isLoggingEnabled()){
            return;
        }
        String log = String.format("✓ Answer: '%s' [%d, %d] -> [%d, %d] | seed: %s ✓\n%s",
                answer.toString().toUpperCase(),
                answer.getLetters().get(0).getXY()[0],
                answer.getLetters().get(0).getXY()[1],
                answer.getLetters().get(answer.getLetters().size()-1).getXY()[0],
                answer.getLetters().get(answer.getLetters().size()-1).getXY()[1], seed.toString(),
                hm.getHintSQLHandler() == null ? "Unexpected error!" : hm.getHintSQLHandler().getLogDecisionLogic());
        cpuLogger.log(log);
    }

    //region private logic steps

    private ArrayList<String> step1_calculateDepartureKeys(ArrayList<int[]> departureLetterXYs,
                                                           ArrayList<String> layout){
        ArrayList<String> departureKeys = new ArrayList<>();
        for(int[] letterXY : departureLetterXYs){
            int[] xyNorth = Util.getNextPlayableMapTileXY(layout, letterXY, Direction.NORTH);
            int[] xyEast  = Util.getNextPlayableMapTileXY(layout, letterXY, Direction.EAST);
            int[] xyWest  = Util.getNextPlayableMapTileXY(layout, letterXY, Direction.WEST);
            int[] xySouth = Util.getNextPlayableMapTileXY(layout, letterXY, Direction.SOUTH);
            if(xyNorth != null){
                String dKey = getDepartureKey(Util.getAdjacentTileXY(layout, xyNorth, Direction.SOUTH),
                        Direction.NORTH);
                if(!departureKeys.contains(dKey)){
                    departureKeys.add(dKey);
                }
            }
            if(xyEast != null){
                String dKey = getDepartureKey(Util.getAdjacentTileXY(layout, xyEast, Direction.WEST),
                        Direction.EAST);
                if(!departureKeys.contains(dKey)){
                    departureKeys.add(dKey);
                }
            }
            if(xyWest != null){
                String dKey = getDepartureKey(Util.getAdjacentTileXY(layout, xyWest, Direction.EAST),
                        Direction.WEST);
                if(!departureKeys.contains(dKey)){
                    departureKeys.add(dKey);
                }
            }
            if(xySouth != null){
                String dKey = getDepartureKey(Util.getAdjacentTileXY(layout, xySouth, Direction.NORTH),
                        Direction.SOUTH);
                if(!departureKeys.contains(dKey)){
                    departureKeys.add(dKey);
                }
            }
        }
        return departureKeys;
    }

    private ArrayList<PathFinderOption> step2_executeAStar(ArrayList<String> departureKeys,
                                                           ArrayList<String> layout,
                                                           int[] targetXY,
                                                           Difficulty difficulty){
        StringBuilder errorLogs = new StringBuilder();
        ArrayList<PathFinderOption> pathFinderOptions = new ArrayList<>();
        for(final String dk : departureKeys){
            final Direction direction = getDepartureDirection(dk);
            final int[] startingXY = Util.getAdjacentTileXY(layout, getDepartureXY(dk), direction);
            if(Util.equal(startingXY, targetXY)){
                //We are right beside the target, this path is perfect
                PathFinderOption pfo = new PathFinderOption(startingXY, direction, new ArrayList<HintWord>(),
                        0, null, null);
                pfo.logData(String.format("AStar Path: [%s,%s] to [%s,%s] | Final Score: 0",
                        startingXY[0], startingXY[1], targetXY[0], targetXY[1]));
                pathFinderOptions.add(pfo);
            }else {
                AStar aStar = new AStar(layout, startingXY, targetXY);
                final List<Node> res = aStar.findPath(direction);
                if (res != null) {
                    final int pathScore = AStar.getPathScore(res);
                    final Pair<int[], Direction> nextPathTurn = AStar.getXyDirectionFirstTurn(res);
                    int[] focusTileXY = Util.getNextPlayableMapTileXY(layout, startingXY, direction);
                    if (focusTileXY != null) {
                        PathFinderOption pfo = new PathFinderOption(startingXY, direction, new ArrayList<HintWord>(), pathScore, nextPathTurn.first, nextPathTurn.second);
                        pfo.logData(aStar.getLogSummary());
                        pathFinderOptions.add(pfo);
                    }else {
                        if(errorLogs.toString().length() > 0){
                            errorLogs.append(", ");
                        }
                        errorLogs.append(dk);
                    }
                }else {
                    if(errorLogs.toString().length() > 0){
                        errorLogs.append(", ");
                    }
                    errorLogs.append(dk);
                }
            }
        }
        if(errorLogs.toString().length() > 0){
            cpuLogger.logAStarErrors("Could not find paths for :" + errorLogs.toString());
        }
        return pathFinderOptions;
    }

    private ArrayList<PathFinderOption> step2_executeAStarWithoutTarget(ArrayList<String> departureKeys,
                                                                        ArrayList<String> layout,
                                                                        Difficulty difficulty) {
        ArrayList<PathFinderOption> pathFinderOptions = new ArrayList<>();
        ArrayList<int[]> targetXYs = Util.getXYsOfAvailablePlayableTilesToPlayer(layout, getDepartureXY(departureKeys.get(0)));
        for(int[] targetXY : targetXYs){
            //First pass, try to aim for tiles not touching any letters, so words will be 3+ letters long & will avoid parallel played words
            if(Util.isNearBoardLetter(layout, targetXY)){
                continue;
            }
            ArrayList<PathFinderOption> pfos = step2_executeAStar(departureKeys, layout, targetXY, difficulty);
            for(PathFinderOption pfo : pfos){
                if(!pathFinderOptions.contains(pfo)){
                    pathFinderOptions.add(pfo);
                }
            }
        }
        if(pathFinderOptions.isEmpty()){
            //Second pass, we will take any tiles we can at this stage
            for(int[] targetXY : targetXYs){
                //First pass, try to aim for tiles not touching any letters, so words will be 3+ letters long & will avoid parallel played words
                ArrayList<PathFinderOption> pfos = step2_executeAStar(departureKeys, layout, targetXY, difficulty);
                for(PathFinderOption pfo : pfos){
                    if(!pathFinderOptions.contains(pfo)){
                        pathFinderOptions.add(pfo);
                    }
                }
            }
        }
        return pathFinderOptions;
    }

    /**
     *
     * @param pathFinderOptions
     * @param isTargetingEndZone Make the AI smarter if this is false. The user may be waiting on the AI to detonate, so don't be slow.
     * @return
     */
    private PathFinderOption step3_choosePath(ArrayList<PathFinderOption> pathFinderOptions,
                                              boolean isTargetingEndZone,
                                              Difficulty difficulty){
        Collections.sort(pathFinderOptions, new Comparator<PathFinderOption>() {
            @Override
            public int compare(PathFinderOption lhs, PathFinderOption rhs) {
                return lhs.getPathScore() - rhs.getPathScore();
            }
        });
        //Path[0] = Strongest. Incrementally get weaker.
        int[] pathIndices;
        switch (difficulty){
            case IMPOSSIBLE: pathIndices = new int[]{0};
                break;
            case VERY_HARD: pathIndices = isTargetingEndZone ?
                    new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 1} :   //90%  chance of picking best path if targeting endzone
                    new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};    //100% chance of picking best path if targeting detonator
                break;
            case HARD: pathIndices = isTargetingEndZone ?
                    new int[]{0, 0, 0, 0, 0, 0, 0, 1, 1, 1} :   //70% chance of picking best path if targeting endzone
                    new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 1};    //90% chance of picking best path if targeting detonator
                break;
            default:
            case NORMAL: pathIndices = isTargetingEndZone ?
                    new int[]{0, 0, 0, 0, 0, 0, 1, 1, 1, 2} :   //60% chance of picking best path if targeting endzone
                    new int[]{0, 0, 0, 0, 0, 0, 0, 0, 1, 1};    //80% chance of picking best path if targeting detonator
                break;
            case EASY: pathIndices = isTargetingEndZone ?
                    new int[]{0, 0, 0, 0, 0, 1, 1, 2, 2, 2} :   //50% chance of picking best path if targeting endzone
                    new int[]{0, 0, 0, 0, 0, 0, 0, 0, 1, 1};    //80% chance of picking best path if targeting detonator
                break;
            case VERY_EASY: pathIndices = isTargetingEndZone ?
                    new int[]{0, 0, 0, 0, 1, 1, 2, 2, 2, 3} :   //40% chance of picking best path if targeting endzone
                    new int[]{0, 0, 0, 0, 0, 0, 0, 0, 1, 1};    //80% chance of picking best path if targeting detonator
                break;
        }
        int pathIndex = pathIndices[getRandom().nextInt(pathIndices.length)];
        if(pathIndex >= pathFinderOptions.size()){
            pathIndex = pathFinderOptions.size() - 1;
        }

        return pathFinderOptions.get(pathIndex);
    }

    private HintManager step4_populatePathWithAnswers(final PathFinderOption pathFinderOption,
                                                      final ArrayList<String> layout,
                                                      String hudLetters,
                                                      double minFrequencyWord) {
        HintManager hm = new HintManager(cpuLogger) {
            @Override
            public void onWordsFound(ArrayList<HintWord> answers) {
                //Eliminate One Tile Short words

                /**
                 *   ..S..  < Playing 'sets' down is bad. It will leave you one tile short.
                 *   ..E..    It would be better to play a 3 letter word (if a 5 letter one is not available)
                 *   ..T..    Playing 'set' instead, would allow the user to play a word east off the t, then move
                 *   --S..    south and then move west to the target.
                 *   1....    Playing sets would have essentially locked the user away from getting to the target.
                 *
                 *   [Exception!!]
                 *   ..S..  < This would be fine, as the user can still play e.g. 'as' to connect to the bottom s and
                 *   ..E..    move towards the target.
                 *   ..T..    One tile short only applies when there is a blocking tile beside the last letter of the word.
                 *   -.S..
                 *   1....
                 */
                if(pathFinderOption.getNextPathTurnDirection() != null){
                    int[] oneTileShortXY = Util.getAdjacentTileXY(layout, pathFinderOption.getNextPathTurnXY(),
                            Util.getOppositeDirection(pathFinderOption.getNextPathTurnDirection()));

                    //One tile short is only a problem if adjacent to a blocking tile
                    if(TileCodes.getBlockTileCodes().contains(Util.getAdjacentCharacter(layout, oneTileShortXY,
                            pathFinderOption.getNextPathTurnDirection()))) {

                        for (int i = answers.size() - 1; i >= 0; i--) {
                            boolean analysingLastLetter = pathFinderOption.getDirection().equals(Direction.EAST) ||
                                    pathFinderOption.getDirection().equals(Direction.SOUTH);
                            HintLetter endLetter = answers.get(i).getLetters().get(analysingLastLetter ? answers.get(i).getLetters().size() - 1 : 0);
                            if (Util.equal(oneTileShortXY, endLetter.getXY())) {
                                //avoid this answer
                                answers.remove(i);
                            }
                        }
                    }

                }

                pathFinderOption.setWords(answers);
            }
        };
        hm.findWordsSynchronously(layout,
                pathFinderOption.getXy(),
                pathFinderOption.getDirection(),
                databaseGateway,
                hudLetters,
                minFrequencyWord);
        return hm;
    }

    private HintWord step5_selectAnswer(PathFinderOption pathFinderOption,
                                        ArrayList<String> layout,
                                        Difficulty difficulty) {
        LinkedHashMap<CPUWordEngineUtils.FILTERS, Float> filters = CPUWordEngineUtils.getFiltersOrdered(difficulty);
        for(Map.Entry<CPUWordEngineUtils.FILTERS, Float> mapFilterValue : filters.entrySet()){
            pathFinderOption = CPUWordEngineUtils.applyFilter(pathFinderOption, layout, mapFilterValue.getKey(), mapFilterValue.getValue());
        }
        HintWord output = null;
        for(HintWord answer : pathFinderOption.getWords()){
            if(output == null){
                output = answer;
            }else {
                if(answer.getTotalDesirability() > output.getTotalDesirability()){
                    output = answer;
                }
            }
        }
        return output;
    }

    //endregion

    //region private helper methods

    private String getDepartureKey(int[] departureXY, Direction direction){
        return departureXY[0] + "|" + departureXY[1] + "|" + direction.ordinal();
    }

    private String getDepartureKeyEnglish(int[] departureXY, Direction direction){
        return "[" + departureXY[0] + ", " + departureXY[1] + "] headed " + direction.name().toLowerCase();
    }

    private int[] getDepartureXY(String departureKey){
        return new int[]{Integer.valueOf(departureKey.split("\\|")[0]), Integer.valueOf(departureKey.split("\\|")[1])};
    }

    private Direction getDepartureDirection(String departureKey){
        return Direction.values()[Integer.valueOf(departureKey.split("\\|")[2])];
    }

    private Random getRandom(){
        return seed == null ? new Random() : new Random(seed);
    }

    //endregion


}

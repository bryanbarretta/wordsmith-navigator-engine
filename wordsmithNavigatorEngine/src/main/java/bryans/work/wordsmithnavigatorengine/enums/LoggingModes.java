package bryans.work.wordsmithnavigatorengine.enums;

public enum LoggingModes {
    NONE,
    LOGCAT,
    DATABASE
}

package bryans.work.wordsmithnavigatorengine.enums;

public enum Colors {

    COLOR_GREEN_LIGHT (0xFF97E779),
    COLOR_BLUE_LIGHT (0xFFDCF6FA),
    COLOR_BLUE (0xFF58BDD8),
    COLOR_BROWN (0xFFC79453),
    COLOR_GREEN (0xFF42AB40),
    COLOR_LT_GREEN (0xFFA5D6A7),
    COLOR_GRAY (0xFF727272),
    COLOR_WHITE (0xFFFFFFFF),
    COLOR_BLACK (0xFF000000),
    COLOR_YELLOW (0xFFF7DF2D),
    COLOR_YELLOW_DARK (0xFFCAC41D),
    COLOR_RED (0xFFF01F1F);

    private int hex;

    Colors(int hex){
        this.hex = hex;
    }

    public int getHexCode(){
        return hex;
    }

}

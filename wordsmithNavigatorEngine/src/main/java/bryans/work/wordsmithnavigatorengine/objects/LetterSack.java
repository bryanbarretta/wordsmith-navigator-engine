package bryans.work.wordsmithnavigatorengine.objects;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Random;

import bryans.work.wordsmithnavigatorengine.level.LevelStaticData;
import bryans.work.wordsmithnavigatorengine.util.Util;

/**
 * Created by Bryan on 12/05/2016.
 */
public class LetterSack {

    private static final int MIN_NUM_VOWELS = 2;
    private static final int MIN_NUM_CONSONANTS = 5;

    private ArrayList<Character> sackVowels;
    private ArrayList<Character> sackConsonants;
    private boolean isHardcodedLetters = false;
    private String excludedLetters = "";
    private final Long seed;
    private Locale locale;

    public LetterSack(LevelStaticData levelStaticData, Locale locale){
        this(levelStaticData, null, locale);
    }

    /**
     * @param levelStaticData
     * @param seed Only to be used in unit tests. Supplying a constant seed means every second letter is always vowel/cons/vowel/cons etc.
     */
    public LetterSack(LevelStaticData levelStaticData, Long seed, Locale locale){
        this.seed = seed;
        this.locale = locale;
        if(!levelStaticData.getHardcodedLetters().isEmpty()){
            sackConsonants = new ArrayList<>();
            for(Character c : levelStaticData.getHardcodedLetters().toCharArray()){
                sackConsonants.add(c);
            }
            isHardcodedLetters = true;
            return;
        } else {
            excludedLetters = levelStaticData.getExcludedLetters();
            populateSack(locale, true, true);
        }
    }

    public Character getLetterBalancingVowelsAndConsonants(int numLettersToReplenish, int numConsonants, int numVowels) {
        if(numVowels >= MIN_NUM_VOWELS && numConsonants >= MIN_NUM_CONSONANTS){
            //Have min vowels and consonants, pure random
            return getRandomLetter();
        }
        else if(numVowels < MIN_NUM_VOWELS && numConsonants >= MIN_NUM_CONSONANTS){
            //Short on vowels, have min consonants
            int vowelsToDistribute = MIN_NUM_VOWELS - numVowels;
            //Still need X vowels
            if(numLettersToReplenish > vowelsToDistribute){
                //We can afford a pure random draw
                return getRandomLetter();
            } else {
                //We have to pick a random vowel, no room for consonants
                return getRandomLetter(true);
            }
        }
        else if(numVowels >= MIN_NUM_VOWELS && numConsonants < MIN_NUM_CONSONANTS){
            //Short on consonants, have min vowels
            int consonantsToDistribute = MIN_NUM_CONSONANTS - numConsonants;
            //Still need X consonants
            if(numLettersToReplenish > consonantsToDistribute){
                //We can afford a pure random draw
                return getRandomLetter();
            } else {
                //We have to pick a random consonant, no room for vowels
                return getRandomLetter(false);
            }
        }else {
            //Not enough tiles to use this algorithm
            return getRandomLetter();
        }
    }

    public Character getRandomLetter(){
        int randomNumber = getRandom().nextInt() +
                (sackConsonants == null ? 0 : sackConsonants.size()) +
                (sackVowels == null ? 0 : sackVowels.size());
        return getRandomLetter((randomNumber % 2) == 0);
    }

    public Character getRandomLetter(boolean isVowel){
        try {
            if(isHardcodedLetters){
                return sackConsonants.remove(0);
            }
            if(isVowel){
                return sackVowels.remove(0);
            }else{
                return sackConsonants.remove(0);
            }
        }catch (IndexOutOfBoundsException e){
            Log.w(LetterSack.this.getClass().toString(), "Out of letters! Going to return " + (isHardcodedLetters ? "null" : "a new sack letter"));
            if(isHardcodedLetters){
                return null;
            }else {
                populateSack(locale, !isVowel, isVowel);
                return getRandomLetter(isVowel);
            }
        }
    }

    //region private

    private void populateSack(Locale region, boolean populateConsonants, boolean populateVowels){
        sackVowels = new ArrayList<>();
        sackConsonants = new ArrayList<>();
        if(region.equals(Locale.ENGLISH)){
            if(populateVowels) {
                if (!excludedLetters.contains("a")) sackVowels.addAll(addLetter('a', 9));
                if (!excludedLetters.contains("e")) sackVowels.addAll(addLetter('e', 12));
                if (!excludedLetters.contains("i")) sackVowels.addAll(addLetter('i', 9));
                if (!excludedLetters.contains("o")) sackVowels.addAll(addLetter('o', 8));
                if (!excludedLetters.contains("u")) sackVowels.addAll(addLetter('u', 4));
            }
            if(populateConsonants) {
                if (!excludedLetters.contains("b")) sackConsonants.addAll(addLetter('b', 2));
                if (!excludedLetters.contains("c")) sackConsonants.addAll(addLetter('c', 2));
                if (!excludedLetters.contains("d")) sackConsonants.addAll(addLetter('d', 4));
                if (!excludedLetters.contains("f")) sackConsonants.addAll(addLetter('f', 2));
                if (!excludedLetters.contains("g")) sackConsonants.addAll(addLetter('g', 3));
                if (!excludedLetters.contains("h")) sackConsonants.addAll(addLetter('h', 2));
                if (!excludedLetters.contains("j")) sackConsonants.addAll(addLetter('j', 1));
                if (!excludedLetters.contains("k")) sackConsonants.addAll(addLetter('k', 1));
                if (!excludedLetters.contains("l")) sackConsonants.addAll(addLetter('l', 4));
                if (!excludedLetters.contains("m")) sackConsonants.addAll(addLetter('m', 2));
                if (!excludedLetters.contains("n")) sackConsonants.addAll(addLetter('n', 6));
                if (!excludedLetters.contains("p")) sackConsonants.addAll(addLetter('p', 6));
                if (!excludedLetters.contains("q")) sackConsonants.addAll(addLetter('q', 1));
                if (!excludedLetters.contains("r")) sackConsonants.addAll(addLetter('r', 6));
                if (!excludedLetters.contains("s")) sackConsonants.addAll(addLetter('s', 4));
                if (!excludedLetters.contains("t")) sackConsonants.addAll(addLetter('t', 6));
                if (!excludedLetters.contains("v")) sackConsonants.addAll(addLetter('v', 2));
                if (!excludedLetters.contains("w")) sackConsonants.addAll(addLetter('w', 2));
                if (!excludedLetters.contains("x")) sackConsonants.addAll(addLetter('x', 1));
                if (!excludedLetters.contains("y")) sackConsonants.addAll(addLetter('y', 2));
                if (!excludedLetters.contains("z")) sackConsonants.addAll(addLetter('z', 1));
            }
        }
        Collections.shuffle(sackVowels, getRandom());
        Collections.shuffle(sackConsonants, getRandom());
    }

    private ArrayList<Character> addLetter(Character character, int quantity){
        ArrayList<Character> a = new ArrayList<>();
        for(int i = 0; i < quantity; i++){
            a.add(character);
        }
        return a;
    }

    private Random getRandom(){
        return seed == null ? new Random() : new Random(seed);
    }

    //endregion

}

package bryans.work.wordsmithnavigatorengine;

import android.content.Context;

import java.util.ArrayList;

import bryans.work.wordsmithnavigatorengine.ai.CPUWordEngineAi;
import bryans.work.wordsmithnavigatorengine.asynctask.AsyncTaskHandler;
import bryans.work.wordsmithnavigatorengine.database.DatabaseGateway;
import bryans.work.wordsmithnavigatorengine.enums.Difficulty;
import bryans.work.wordsmithnavigatorengine.logs.CpuLogger;
import bryans.work.wordsmithnavigatorengine.logs.CpuLoggerInterface;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;

public abstract class WordNavEngine {

    private Context context;
    private Long seed;
    private DatabaseGateway databaseGateway;
    private CpuLoggerInterface cpuLogger;
    private CPUWordEngineAi cpuWordEngineAi;

    public WordNavEngine(Context context){
        initialise(new DatabaseGateway(context), null, null, false);
    }

    public WordNavEngine(Context context, Long seed) {
        initialise(new DatabaseGateway(context), seed, null, false);
    }

    public WordNavEngine(Context context, boolean logInDatabase) {
        initialise(new DatabaseGateway(context), null, null, logInDatabase);
    }

    public WordNavEngine(Context context, Long seed, boolean logInDatabase) {
        initialise(new DatabaseGateway(context), seed, null, logInDatabase);
    }

    public WordNavEngine(Context context, Long seed, CpuLoggerInterface cpuLogger){
        initialise(new DatabaseGateway(context), seed, cpuLogger, false);
    }

    public WordNavEngine(DatabaseGateway databaseGateway, Long seed, CpuLoggerInterface cpuLogger){
        initialise(databaseGateway, seed, cpuLogger, false);
    }

    private void initialise(DatabaseGateway databaseGateway, Long seed, CpuLoggerInterface cpuLogger, boolean logInDatabase){
        this.context = context;
        this.seed = seed;
        this.databaseGateway = databaseGateway;
        this.cpuLogger = cpuLogger;
        if(logInDatabase){
            cpuLogger = new CpuLogger(databaseGateway);
        }
        this.cpuWordEngineAi = new CPUWordEngineAi(databaseGateway, seed, cpuLogger) {
            @Override
            public void onWordReady(HintWord word) {
                WordNavEngine.this.onWordReady(word);
            }
        };
    }

    public void findNextWord(AsyncTaskHandler asyncTaskHandler,
                             final ArrayList<HintWord> playedWords,
                             final ArrayList<String> layout,
                             final int[] targetXY,
                             final String hudLetters,
                             final Difficulty difficulty,
                             final double minFrequencyWord){
        cpuWordEngineAi.findNextWord(asyncTaskHandler, playedWords, layout, targetXY, hudLetters, difficulty, minFrequencyWord);
    }

    public abstract void onWordReady(HintWord word);

}

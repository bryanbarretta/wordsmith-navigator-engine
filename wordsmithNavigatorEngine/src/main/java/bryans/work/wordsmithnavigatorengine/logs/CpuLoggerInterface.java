package bryans.work.wordsmithnavigatorengine.logs;

import java.util.ArrayList;

import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.objects.PathFinderOption;

public interface CpuLoggerInterface {

    public boolean isLoggingEnabled();

    public void logPlayedWords(ArrayList<HintWord> playedWords);

    public void logTargetXy(int[] targetXY);

    public void logDepartureKeys(ArrayList<String> departureKeys);

    public void logAStarData(ArrayList<PathFinderOption> pathFinderOptions);

    public void logAStarErrors(String errorLogs);

    public void logPathFinderOptionEvaluation(String log);

    public void logAnswer(HintWord answer);

    public void logLayout();

    public void log(String log);

}

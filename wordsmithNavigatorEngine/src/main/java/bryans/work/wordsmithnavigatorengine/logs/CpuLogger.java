package bryans.work.wordsmithnavigatorengine.logs;

import org.joda.time.DateTime;

import java.util.ArrayList;

import bryans.work.wordsmithnavigatorengine.database.DatabaseGateway;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.objects.PathFinderOption;

/**
 * Optional logger to record the logic behind every decision the CPU makes
 * Created by Bryan on 16/06/2018.
 */

public class CpuLogger implements CpuLoggerInterface{

    private CpuGameLogs gameLogs;
    private boolean loggingEnabled = false;

    public CpuLogger(){
        this(null);
    }

    public CpuLogger(DatabaseGateway db){
        this.gameLogs = new CpuGameLogs(generateGameName(), db != null);
        if(db != null){
            enableWriteToDatabase(db);
        }
    }

    //region private

    private String generateGameName() {
        return DateTime.now() + "_wne";
    }

    private int getTurn(){
        return -1; //gameActivity.getPlayerManager().getPlayer(PlayerType.CPU).getPlayedWords().size();
    }

    //endregion

    public boolean enableWriteToDatabase(DatabaseGateway db){
        return gameLogs.enableWriteToDatabase(db);
    }

    @Override
    public boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    public void logPlayedWords(ArrayList<HintWord> playedWords){
        gameLogs.log(getTurn(), CpuTurnLog.LogType.PlayedWords, "Played Words: " + android.text.TextUtils.join(", ", playedWords));
    }

    public void logTargetXy(int[] targetXY){
        gameLogs.log(getTurn(), CpuTurnLog.LogType.TargetXy, "Target XY: [" + targetXY[0] + ", " + targetXY[1] + "]");
    }

    public void logDepartureKeys(ArrayList<String> departureKeys) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.DepartureKeys, "Departure Keys [X|Y|DirectionOrdinalNEWS]: " + android.text.TextUtils.join(", ", departureKeys));
    }

    public void logAStarData(ArrayList<PathFinderOption> pathFinderOptions) {
        StringBuilder data = new StringBuilder();
        int limit = 5;
        int i = 0;
        for(PathFinderOption p : pathFinderOptions){
            if(i >= limit) break;
            data.append((data.length() < 1 ? "" : "\n") + pathFinderOptions.indexOf(p) + ") " + p.getLogDataAStar());
            i++;
        }
        gameLogs.log(getTurn(), CpuTurnLog.LogType.AStar, data.toString());
    }

    public void logAStarErrors(String errorLogs) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.AStarErrors, errorLogs);
    }

    public void logPathFinderOptionEvaluation(String log) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.PfoEvaluation, log);
    }

    public void logAnswer(HintWord answer) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.Answer, answer.toString());
    }

    public void logLayout() {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.Layout, "logLayout not implemented");
        //gameLogs.log(getTurn(), CpuTurnLog.LogType.Layout, LevelUtils.getLevelAsString(gameActivity.getLevelManager().getLayoutModified()));
    }

    @Override
    public void log(String log) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.General, log);
    }
}

package bryans.work.wordsmithnavigatorengine;

import org.junit.Test;

import java.util.ArrayList;

import bryans.work.wordsmithnavigatorengine.enums.Difficulty;
import bryans.work.wordsmithnavigatorengine.enums.TileCodes;
import bryans.work.wordsmithnavigatorengine.objects.HintLetter;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.superclass.BaseWETest;
import bryans.work.wordsmithnavigatorengine.util.Util;

import static junit.framework.TestCase.assertTrue;

/**
 * NOTE: To fix "missing manifest"/null dictionary error:
 *       Click dropdown left of Run button
 *       Edit Configurations...
 *       Under Configuration tab, click File button to right of Working Directory field
 *       Click MODULE_DIR and run tests again
 * Created by Bryan on 04/05/2017.
 */

public class SimpleWETest extends BaseWETest {

    @Override
    public boolean isTestAssertionEnabled() {
        return true;
    }

    @Override
    public boolean isPrintTestResultsEnabled() {
        return true;
    }

    @Override
    public boolean isLogCpuLoggerViaSystemOutPrintEnabled() {
        return true;
    }

    @Override
    public boolean isLogTextFileToDirEnabled() {
        return false;
    }

    @Test
    public void testSimpleEast() throws InterruptedException {
        testSimpleEast(Difficulty.VERY_EASY, "some");
        testSimpleEast(Difficulty.EASY,      "some");
        testSimpleEast(Difficulty.NORMAL,    "tongue");
        testSimpleEast(Difficulty.HARD,      "tongue");
        testSimpleEast(Difficulty.VERY_HARD, "toughen");
        testSimpleEast(Difficulty.IMPOSSIBLE,"toughen");
    }

    @Test
    public void testSimpleSouth() throws InterruptedException {
        testSimpleSouth(Difficulty.VERY_EASY, "some");
        testSimpleSouth(Difficulty.EASY,      "some");
        testSimpleSouth(Difficulty.NORMAL,    "samuel");
        testSimpleSouth(Difficulty.HARD,      "samuel");
        testSimpleSouth(Difficulty.VERY_HARD, "samuel");
        testSimpleSouth(Difficulty.IMPOSSIBLE,"shoeman");
    }

    @Test
    public void testSimpleWest() throws InterruptedException {
        testSimpleWest(Difficulty.VERY_EASY, "has");
        testSimpleWest(Difficulty.EASY,      "means");
        testSimpleWest(Difficulty.NORMAL,    "amount");
        testSimpleWest(Difficulty.HARD,      "amount");
        testSimpleWest(Difficulty.VERY_HARD, "hangout");
        testSimpleWest(Difficulty.IMPOSSIBLE,"hangout");
    }

    @Test
    public void testSimpleNorth() throws InterruptedException {
        testSimpleNorth(Difficulty.VERY_EASY, "has");
        testSimpleNorth(Difficulty.EASY,      "means");
        testSimpleNorth(Difficulty.NORMAL,    "holmes");
        testSimpleNorth(Difficulty.HARD,      "angelus");
        testSimpleNorth(Difficulty.VERY_HARD, "manholes");
        testSimpleNorth(Difficulty.IMPOSSIBLE,"manholes");
    }

    //region methods

    private void testSimpleEast(Difficulty difficulty, String expectedAnswer) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-T0.............1-");
        layout.add("-E..............1-");
        layout.add("-S..............1-");
        layout.add("-T..............1-");
        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(1, 0, 't', true, false));
        playedWord.addLetter(new HintLetter(1, 1, 'e', true, false));
        playedWord.addLetter(new HintLetter(1, 2, 's', true, false));
        playedWord.addLetter(new HintLetter(1, 3, 't', true, false));
        HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                Util.getFirstXYof(layout, TileCodes.EndZone.getCharacter()),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer));
    }

    private void testSimpleSouth(Difficulty difficulty, String expectedAnswer) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-TEST-");
        layout.add("-0...-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-1111-");
        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(0, 0, 't', true, false));
        playedWord.addLetter(new HintLetter(1, 0, 'e', true, false));
        playedWord.addLetter(new HintLetter(2, 0, 's', true, false));
        playedWord.addLetter(new HintLetter(3, 0, 't', true, false));
        HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                Util.getFirstXYof(layout, TileCodes.EndZone.getCharacter()),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer));
    }

    private void testSimpleWest(Difficulty difficulty, String expectedAnswer) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-1.............0T-");
        layout.add("-1..............E-");
        layout.add("-1..............S-");
        layout.add("-1..............T-");
        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(16, 0, 't', true, false));
        playedWord.addLetter(new HintLetter(16, 1, 'e', true, false));
        playedWord.addLetter(new HintLetter(16, 2, 's', true, false));
        playedWord.addLetter(new HintLetter(16, 3, 't', true, false));
        HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                Util.getFirstXYof(layout, TileCodes.EndZone.getCharacter()),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer));
    }

    private void testSimpleNorth(Difficulty difficulty, String expectedAnswer) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-1111-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-0...-");
        layout.add("-TEST-");
        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(0, 13, 't', true, false));
        playedWord.addLetter(new HintLetter(1, 13, 'e', true, false));
        playedWord.addLetter(new HintLetter(2, 13, 's', true, false));
        playedWord.addLetter(new HintLetter(3, 13, 't', true, false));
        HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                Util.getFirstXYof(layout, TileCodes.EndZone.getCharacter()),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer));
    }

    //endregion
}

package bryans.work.wordsmithnavigatorengine.superclass;

import java.util.ArrayList;

import bryans.work.wordsmithnavigatorengine.enums.Difficulty;
import bryans.work.wordsmithnavigatorengine.objects.HintLetter;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.util.Util;

import static junit.framework.TestCase.assertTrue;

/**
 * Base Word Engine Level test.
 * Expands upon BaseWETest. Rather than just play one word, simulates the rest of the words the CPU will play until level finishes.
 */
public abstract class BaseWELevelTest extends BaseWETest {

    public abstract ArrayList<String> getLayout();

    public abstract String getHudLetters();

    private ArrayList<HintWord> playedWords = new ArrayList<>();

    private void executeTest(Difficulty difficulty, String[] expectedAnswers){
        populatePlayedWords();
        ArrayList<String> layout = getLayout();
        String tempHudLetters = getHudLetters();
        String hudLetters = tempHudLetters.length() >= 10 ? tempHudLetters.substring(0, 10) : tempHudLetters;
        tempHudLetters = tempHudLetters.replaceFirst(hudLetters, "");

        boolean hasReachedTarget = false;
        int iAnswer = 0;
        while (!hasReachedTarget) {
            if(answer != null){
                playedWords.add(answer);
                layout = Util.applyHintWordToLevel(layout, answer);
                for(HintLetter l : answer.getLetters()){
                    hudLetters.replaceFirst(l.getLetter().toString(), "");
                }
                int countNeeded = 10 - hudLetters.length();
                String newLetters = tempHudLetters.length() >= countNeeded ? tempHudLetters.substring(0, countNeeded) : tempHudLetters;
                hudLetters += newLetters;
                tempHudLetters.replaceFirst(newLetters, "");
            }
            answer = null;
            int[] targetXY = Util.getOptimalTargetXY(layout, playedWords.get(playedWords.size() - 1).getLetters()
                    .get(playedWords.get(playedWords.size() - 1).getLetters().size() - 1).getXY());
            answer = getNextCPUWord(playedWords, layout, targetXY, hudLetters, difficulty);
            if(answer == null){
                System.out.println("! Cannot think of a word => Forfeit !");
                break;
            }
            if (isPrintTestResultsEnabled()) {
                log(difficulty, answer);
                System.out.println(Util.getLevelAsString(layout) + "\n");
            }
            if (isTestAssertionEnabled()) {
                assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswers[iAnswer]));
                iAnswer++;
            }
            break;
        }
    }

    private void populatePlayedWords(){
        ArrayList<int[]> alreadyEvaluatedLettersXYs = new ArrayList<>();
        for(int y = 0; y < getLayout().size(); y++){
            String row = getLayout().get(y);
            for(int x = 0; x < row.length(); x++){
                Character c = row.charAt(x);
                int[] xy = new int[]{x, y};
                if(Character.isLetter(c) && !Util.contains(alreadyEvaluatedLettersXYs, xy)){
                    HintWord word = Util.getInitialBoardWord(databaseGateway, xy, getLayout());
                    if(!playedWords.contains(word)){
                        playedWords.add(word);
                        for(HintLetter l : word.getLetters()){
                            alreadyEvaluatedLettersXYs.add(l.getXY());
                        }
                    }
                }
            }
        }
    }

}

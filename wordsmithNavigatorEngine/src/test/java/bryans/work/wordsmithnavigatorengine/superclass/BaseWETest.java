package bryans.work.wordsmithnavigatorengine.superclass;

import android.app.Activity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import bryans.work.wordsmithnavigatorengine.BuildConfig;
import bryans.work.wordsmithnavigatorengine.R;
import bryans.work.wordsmithnavigatorengine.WordNavEngine;
import bryans.work.wordsmithnavigatorengine.asynctask.AsyncTaskHandler;
import bryans.work.wordsmithnavigatorengine.database.DatabaseGateway;
import bryans.work.wordsmithnavigatorengine.enums.Difficulty;
import bryans.work.wordsmithnavigatorengine.enums.SupportedLanguages;
import bryans.work.wordsmithnavigatorengine.mock.MockCpuLogger;
import bryans.work.wordsmithnavigatorengine.objects.DictionaryEntry;
import bryans.work.wordsmithnavigatorengine.objects.HintLetter;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.util.Configuration;
import bryans.work.wordsmithnavigatorengine.util.Util;

/**
 * Base Word Engine test.
 * Sets up dictionary. Call getNextCPUWord() to see the next word the computer will play.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public abstract class BaseWETest {

    public abstract boolean isTestAssertionEnabled();

    public abstract boolean isPrintTestResultsEnabled();

    public abstract boolean isLogCpuLoggerViaSystemOutPrintEnabled();

    public abstract boolean isLogTextFileToDirEnabled();

    protected final long SEED = 291304L;
    private boolean FLAG_WAIT = false;

    protected Activity activity;
    protected DatabaseGateway databaseGateway;
    protected WordNavEngine wordNavEngine;
    protected HintWord answer = null;
    protected MockCpuLogger mockCpuLogger;
    long startTime;

    @Rule
    public TestRule watcher = new TestWatcher() {
        protected void starting(Description description) {
            System.out.println("\nStarting test: " + description.getMethodName());
        }
    };

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(Activity.class)
                .create()
                .resume()
                .get();
        databaseGateway = new DatabaseGateway(activity);
        ArrayList<DictionaryEntry> entries = new ArrayList<>();
        InputStream dict = activity.getResources().openRawResource(R.raw.dictionary_en_uk);
        InputStreamReader isr = new InputStreamReader(dict, Charset.forName("UTF-8"));
        BufferedReader br = new BufferedReader(isr);
        String line = "";
        try {
            double mostFreq = SupportedLanguages.ENGLISH_UK.getMostFrequentWordCount();
            double factor = Util.getFactor(mostFreq);
            while ((line = br.readLine()) != null) {
                String[] parts = line.split(",");
                String displayWord = parts[0].toLowerCase();
                String baseWord = Util.getBaseWord(displayWord);
                double freq = parts.length > 1 ? Double.valueOf(parts[1]) : 0;
                entries.add(new DictionaryEntry(baseWord,
                        displayWord.equals(baseWord) ? null : displayWord,
                        freq <= 0 ? 0 : Math.round((freq / mostFreq) * factor) / factor));
            }
            br.close();
            dict.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        databaseGateway.addUpdateDictionaryEntries(entries);
        FLAG_WAIT = true;
        answer = null;
        String logToFileDirPath = isLogTextFileToDirEnabled() ?
            Configuration.getInstance(activity).getStringValue("UNIT_TEST_OUTPUT_DIR") : null;
        mockCpuLogger = new MockCpuLogger(isLogCpuLoggerViaSystemOutPrintEnabled(), logToFileDirPath);
        wordNavEngine = new WordNavEngine(databaseGateway, this.SEED, mockCpuLogger) {
            @Override
            public void onWordReady(HintWord word) {
                answer = word;
                FLAG_WAIT = false;
            }
        };
        startTime = System.currentTimeMillis();
    }

    @After
    public void endOfTest(){
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("TOTAL TEST TIME: " + elapsedTime + " ms");
    }

    /**
     * Gets the next word the CPU would play.
     * Synchronous task which uses Thread.wait() so the test will not finish too early.
     * @param playedWords   List of words already on the board in order played
     * @param layout        Current layout of the level
     * @param targetXY      Target Tile's column & row
     * @param hudLetters    Current tile rack letters
     * @param difficulty    This along with SEED determine the answer
     * @return Null if a word cannot be found. Otherwise return the CPU's answer
     */
    protected HintWord getNextCPUWord(ArrayList<HintWord> playedWords, ArrayList<String> layout, int[] targetXY, String hudLetters, Difficulty difficulty){
        FLAG_WAIT = true;
        wordNavEngine.findNextWord(new AsyncTaskHandler(), playedWords, layout, targetXY, hudLetters, difficulty, difficulty.getMinFrequencyWord(SupportedLanguages.ENGLISH_UK));
        while (FLAG_WAIT){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return answer;
    }

    protected void log(Difficulty difficulty, HintWord word){
        String msg = difficulty.name() + " = " + word.getDictionaryEntry().getWord() + " [";
        for(HintLetter l : word.getLetters()){
            if(msg.endsWith("}")){
                msg += ", ";
            }
            msg += "{" + l.getXY()[0] + ", "+ l.getXY()[1] + "}";
        }
        msg += "]";
        System.out.println(msg);
    }

}

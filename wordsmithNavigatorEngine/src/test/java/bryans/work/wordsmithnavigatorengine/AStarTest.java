package bryans.work.wordsmithnavigatorengine;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import bryans.work.wordsmithnavigatorengine.astar.AStar;
import bryans.work.wordsmithnavigatorengine.astar.Node;
import bryans.work.wordsmithnavigatorengine.enums.Difficulty;
import bryans.work.wordsmithnavigatorengine.enums.Direction;
import bryans.work.wordsmithnavigatorengine.util.Util;

public class AStarTest {

    /**
     * Penalties:
     * HV*(10 points) - standard penalty cost, needed for algorithm to work
     * LS (20 points) - Long Straight penalty; for each additional letter in the same direction after an ideal word length (based on difficulty), penalty is COST_LS * surplusLetterCount
     * TN*(30 points) - turn cost penalty, designed to promote long straight paths (and avoid 'stair' pattern paths)
     * AL*(50 points) - adjacent letter cost, try to avoid paths that will run parallel to letters
     * ED (500 points)- embark direction cost, apply this if the path from the initial node is not in the direction specified (embarkDirection)
     */

    @Test
    public void testTurnPenalty(){
        ArrayList<String> layout = new ArrayList<>();
        layout.add("S......");
        layout.add("...-...");
        layout.add("...-...");
        layout.add("...-...");
        layout.add(".......");
        layout.add("......1");

        ArrayList<String> expectedVeryEasy = new ArrayList<>();
        expectedVeryEasy.add("*****..");
        expectedVeryEasy.add("...-*..");
        expectedVeryEasy.add("...-*..");
        expectedVeryEasy.add("...-*..");
        expectedVeryEasy.add("....***");
        expectedVeryEasy.add("......*");

        ArrayList<String> expectedVeryHard = new ArrayList<>();
        expectedVeryHard.add("*******");
        expectedVeryHard.add("...-..*");
        expectedVeryHard.add("...-..*");
        expectedVeryHard.add("...-..*");
        expectedVeryHard.add("......*");
        expectedVeryHard.add("......*");

        AStar aStar1 = new AStar(layout, new int[]{0, 0}, Util.getFirstXYof(layout, '1'), Difficulty.VERY_EASY);
        AStar aStar2 = new AStar(layout, new int[]{0, 0}, Util.getFirstXYof(layout, '1'), Difficulty.VERY_HARD);

        Assert.assertEquals(Util.getLevelAsString(expectedVeryEasy), Util.getLevelAsString(applyPath(layout, aStar1.findPath(Direction.EAST))));
        Assert.assertEquals(Util.getLevelAsString(expectedVeryHard), Util.getLevelAsString(applyPath(layout, aStar2.findPath(Direction.EAST))));
    }

    @Test
    public void testLetterPenalty(){
        ArrayList<String> layout = new ArrayList<>();
        layout.add("S.....T");
        layout.add("......E");
        layout.add("......N");
        layout.add(".......");
        layout.add(".......");
        layout.add("......1");

        ArrayList<String> expected1 = new ArrayList<>();
        expected1.add("*****.T");
        expected1.add("....*.E");
        expected1.add("....*.N");
        expected1.add("....*..");
        expected1.add("....*..");
        expected1.add("....***");

        ArrayList<String> expected2 = new ArrayList<>();
        expected2.add("*.....T");
        expected2.add("*.....E");
        expected2.add("*.....N");
        expected2.add("*......");
        expected2.add("*......");
        expected2.add("*******");

        AStar aStar1 = new AStar(layout, new int[]{0, 0}, Util.getFirstXYof(layout, '1'));
        AStar aStar2 = new AStar(layout, new int[]{0, 0}, Util.getFirstXYof(layout, '1'));

        Assert.assertEquals(Util.getLevelAsString(expected1), Util.getLevelAsString(applyPath(layout, aStar1.findPath(Direction.EAST))));
        Assert.assertEquals(Util.getLevelAsString(expected2), Util.getLevelAsString(applyPath(layout, aStar2.findPath(Direction.SOUTH))));
    }

    @Test
    public void testLongStraightPenalty(){

        ArrayList<String> layout = new ArrayList<>();
        layout.add("S..................1");
        layout.add("....................");
        layout.add("....................");
        layout.add("....................");
        layout.add("....................");
        layout.add("....................");

        ArrayList<String> expected1 = new ArrayList<>();
        expected1.add("******....******...*");
        expected1.add(".....******....*****");
        expected1.add("....................");
        expected1.add("....................");
        expected1.add("....................");
        expected1.add("....................");

        AStar aStar = new AStar(layout, new int[]{0, 0}, Util.getFirstXYof(layout, '1'));

        Assert.assertEquals(Util.getLevelAsString(expected1), Util.getLevelAsString(applyPath(layout, aStar.findPath(Direction.EAST))));
    }

    //region private

    private ArrayList<String> applyPath(ArrayList<String> layout, List<Node> path){
        ArrayList<String> output = (ArrayList<String>)layout.clone();
        for(Node n : path){
            String row = output.get(n.getRow());
            String updated = row.substring(0, n.getCol()) + "*" + row.substring(n.getCol() + 1);
            output.set(n.getRow(), updated);
        }
        return output;
    }

    //endregion
}

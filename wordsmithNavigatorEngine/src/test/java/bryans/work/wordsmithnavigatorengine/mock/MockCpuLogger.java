package bryans.work.wordsmithnavigatorengine.mock;

import android.util.Log;

import org.codehaus.plexus.util.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import bryans.work.wordsmithnavigatorengine.logs.CpuLoggerInterface;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.objects.PathFinderOption;

public class MockCpuLogger implements CpuLoggerInterface {

    private final boolean logViaSystemOutPrint;
    private String logTextFilesToDir;
    private String currentTestTag;
    private String PREFIX_OUTPUT_FOLDER = "WETests_";

    public MockCpuLogger(boolean logViaSystemOutPrint){
        this(logViaSystemOutPrint, null);
    }

    public MockCpuLogger(boolean logViaSystemOutPrint, String logTextFilesToDir){
        this.logViaSystemOutPrint = logViaSystemOutPrint;
        if(logTextFilesToDir != null){
            //Delete older WETest_ folders as they can be big
            File dir = new File(logTextFilesToDir);
            for(File f : dir.listFiles()){
                if(f.isDirectory() && f.getName().startsWith(PREFIX_OUTPUT_FOLDER)){
                    if(!f.delete()){
                        try {
                            FileUtils.deleteDirectory(f);
                        } catch (Exception e) {
                            Log.e(MockCpuLogger.class.getSimpleName(), "Unable to delete " + f.getName());
                        }
                    }
                }
            }
            //Prepare
            StringBuilder sb = new StringBuilder(logTextFilesToDir);
            if(!logTextFilesToDir.endsWith("\\")){
                sb.append("\\");
            }
            sb.append(PREFIX_OUTPUT_FOLDER + DateTime.now().toString(DateTimeFormat.forPattern("yyyyMMdd_HHmmss")));
            this.logTextFilesToDir = sb.toString();
        }
    }

    @Override
    public boolean isLoggingEnabled() {
        return logViaSystemOutPrint;
    }

    @Override
    public void logPlayedWords(ArrayList<HintWord> playedWords) {
        if(logViaSystemOutPrint) {
            write("Played Words: " + android.text.TextUtils.join(", ", playedWords));
        }
    }

    @Override
    public void logTargetXy(int[] targetXY) {
        if(logViaSystemOutPrint) {
            write("Target XY: [" + (targetXY == null ? "?" : targetXY[0])
                                                      + ", " + (targetXY == null ? "?" : targetXY[1]) + "]");
        }
    }

    @Override
    public void logDepartureKeys(ArrayList<String> departureKeys) {
        write("Departure Keys [X|Y|DirectionOrdinalNEWS]: " + android.text.TextUtils.join(", ", departureKeys));
    }

    @Override
    public void logAStarData(ArrayList<PathFinderOption> pathFinderOptions) {
        StringBuilder data = new StringBuilder();
        int limit = 5;
        int i = 0;
        for(PathFinderOption p : pathFinderOptions){
            if(i >= limit) break;
            if(p.getLogDataAStar() == null){
                int a = 0;
                int x = a;
            }
            data.append((data.length() < 1 ? "" : "\n") + pathFinderOptions.indexOf(p) + ") " + p.getLogDataAStar());
            i++;
        }
        write(data.toString() + (i >= limit ? "\n...and " + (pathFinderOptions.size() - limit) + " more" : ""));
    }

    @Override
    public void logAStarErrors(String errorLogs) {
        if(logViaSystemOutPrint) {
            write(errorLogs);
        }
    }

    @Override
    public void logPathFinderOptionEvaluation(String log) {
        if(logViaSystemOutPrint) {
            write(log);
        }
    }

    @Override
    public void logAnswer(HintWord answer) {
        if(logViaSystemOutPrint) {
            write(answer.toString());
        }
    }

    @Override
    public void logLayout() {

    }

    @Override
    public void log(String log) {
        write(log);
    }

    public void setCurrentTestTag(String currentTestTag){
        this.currentTestTag = currentTestTag;
    }

    private void write(String log){
        if(logViaSystemOutPrint) {
            System.out.println(log);
        }
        if(logTextFilesToDir != null && currentTestTag != null){
            writeToFile(logTextFilesToDir, currentTestTag + ".txt", log);
        }
    }

    private void writeToFile(String dir, String fileNameOnly, String value){
        File directory = new File(dir);
        if (!directory.exists()){
            if(!directory.mkdirs()){
                throw new RuntimeException("MKDirs Permission denied");
            }
        }
        try(FileWriter fw = new FileWriter(dir + "/" + fileNameOnly, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(value);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

}

package bryans.work.wordsmithnavigatorengine;

import org.junit.Ignore;
import org.junit.Test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

import bryans.work.wordsmithnavigatorengine.enums.Difficulty;
import bryans.work.wordsmithnavigatorengine.enums.PlayerType;
import bryans.work.wordsmithnavigatorengine.enums.SupportedLanguages;
import bryans.work.wordsmithnavigatorengine.enums.TileCodes;
import bryans.work.wordsmithnavigatorengine.level.Level;
import bryans.work.wordsmithnavigatorengine.level.LevelFactory;
import bryans.work.wordsmithnavigatorengine.level.rules.DetonatorLink;
import bryans.work.wordsmithnavigatorengine.level.rules.GateLockPoints;
import bryans.work.wordsmithnavigatorengine.objects.DictionaryEntry;
import bryans.work.wordsmithnavigatorengine.objects.HintLetter;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.objects.LetterSack;
import bryans.work.wordsmithnavigatorengine.objects.SessionStatistics;
import bryans.work.wordsmithnavigatorengine.objects.TestedLevelSummary;
import bryans.work.wordsmithnavigatorengine.superclass.BaseWETest;
import bryans.work.wordsmithnavigatorengine.util.Util;

/**
 * This test suite will go through every level and ensure the CPU won't get stuck.
 */
public class SimulateLevelsWETest extends BaseWETest {

    private static final boolean LOG_CPU_LOGGER_LOGIC = true;
    private static final boolean LOG_TEXT_FILE_TO_DIR = false; //If you want to generate text files

    ArrayList<Level> levels;

    private static final String LOG_TICKS     = "✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓";
    private static final String LOG_FAILED_LV = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

    ArrayList<TestedLevelSummary> levelSummaries;

    @Test
    public void testLevels() throws InterruptedException {
        mockCpuLogger.log("Preparing...");
        levelSummaries = new ArrayList<>();
        LevelFactory lf = new LevelFactory(activity.getAssets(), SupportedLanguages.ENGLISH_UK);
        levels = lf.getLevels(1, lf.getTotalLevelCount()); //lf.getTotalLevelCount()
        ArrayList<Difficulty> difficultiesToTest = new ArrayList<Difficulty>(){
            {add(Difficulty.VERY_EASY);}
            {add(Difficulty.EASY);}
            {add(Difficulty.NORMAL);}
            {add(Difficulty.HARD);}
            {add(Difficulty.VERY_HARD);}
            {add(Difficulty.IMPOSSIBLE);}
        };
        int levelToTest = -1; //specify the level num tested. -1 will test all levels
        ArrayList<PlayerType> playersToTest = new ArrayList<PlayerType>(){
            {add(PlayerType.USER);}
            {add(PlayerType.CPU);}
        };
        logTestPreview(levelToTest, levels, difficultiesToTest, playersToTest);
        executeTestLevels(difficultiesToTest, playersToTest, levelToTest);
        logTestResults();
    }

    //region private

    private void executeTestLevels(ArrayList<Difficulty> difficultiesToTest, ArrayList<PlayerType> playersToTest, int levelToTest){
        for(Level level : levels){
            boolean levelEvaluated = false;
            for(PlayerType player : playersToTest) {
                for(Difficulty difficulty : difficultiesToTest) {
                    if (levelToTest > 0) {
                        if(level.getLevelStaticData().getId() == levelToTest) {
                            levelEvaluated = executeTestSpecificLevel(difficulty, player, level);
                        }else {
                            continue;
                        }
                    }else {
                        levelEvaluated = executeTestSpecificLevel(difficulty, player, level);
                    }
                }
            }
            if(levelEvaluated) {
                logBreak();
            }
        }
    }

    private boolean executeTestSpecificLevel(Difficulty difficulty, PlayerType player, Level level){
        mockCpuLogger.setCurrentTestTag(String.format("Level%s_%s_%s",
                level.getLevelStaticData().getId(), player.name(), difficulty.name()));
        ArrayList<String> layoutModified = new ArrayList<>(level.getLevelStaticData().getLayoutOriginal());
        if(!Util.getLevelAsString(layoutModified).contains(player.getTileCode().getCharacter().toString())){
            return false;
        }
        logLevelTitle(level, difficulty, player);
        String logGateInfo = "";
        if(!level.getLevelStaticData().getPointsLocks().isEmpty()){
            for(GateLockPoints g : level.getLevelStaticData().getPointsLocks()){
                logGateInfo += (logGateInfo.isEmpty() ? "" : " | ") + g.getOriginalPoints();
            }
        }
        int maxNumLetters = 10;
        int[] currentXY = Util.getFirstXYof(layoutModified, player.getTileCode());
        LetterSack letterSack = new LetterSack(level.getLevelStaticData(), 204963L, Locale.ENGLISH); //Got nice seed running testLetterSacks
        String hudLetters = "";
        for(int i = 0; i < maxNumLetters; i++){
            hudLetters += letterSack.getLetterBalancingVowelsAndConsonants(maxNumLetters - i,
                    Util.getConsonantsCount(hudLetters),
                    Util.getVowelsCount(hudLetters));
        }
        log(difficulty, player, level.getLevelStaticData().getId(), String.format("Beginning test: {hud: %s}", hudLetters));
        final HintWord startingWord = Util.getInitialBoardWord(databaseGateway, currentXY, layoutModified);
        ArrayList<HintWord> playedWords = new ArrayList<HintWord>(){
            {add(startingWord);}
        };
        int[] targetXY = Util.getOptimalTargetXY(layoutModified, currentXY);
        SessionStatistics sessionStatistics = new SessionStatistics(player.ordinal());
        boolean forfeited = false;
        int turn = 1;
        while(true){
            log(difficulty, player, level.getLevelStaticData().getId(), String.format("Starting Turn " + turn + " ###"));
            answer = getNextCPUWord(playedWords, layoutModified, targetXY, hudLetters, difficulty);
            if(answer == null){
                log(difficulty, player, level.getLevelStaticData().getId(),
                        String.format("Unable to complete level, FORFEIT (Points: %s, Words: %s)\n%s\n%s",
                                sessionStatistics.getTotalPoints()+"", sessionStatistics.getSubmittedWords().size()+"", Util.getLevelAsString(layoutModified).trim(), LOG_FAILED_LV));
                forfeited = true;
                break;
            }
            layoutModified = Util.applyHintWordToLevel(layoutModified, answer);
            for(HintLetter l : answer.getLetters()){
                l.setLockedOnBoard(true);
                l.setFocusTile(false);
            }
            playedWords.add(answer);
            sessionStatistics.addWord(answer.toString(), Util.getPointsForWord(answer.toString()));
            log(difficulty, player, level.getLevelStaticData().getId(), String.format("Playing '%s':\t[%d,%d] -> [%d,%d]. Points: %s.%s",
                    answer.toString(),
                    answer.getFirstLetter().getXY()[0], answer.getFirstLetter().getXY()[1],
                    answer.getLastLetter().getXY()[0], answer.getLastLetter().getXY()[1],
                    sessionStatistics.getTotalPoints()+"",
                    logGateInfo.isEmpty() ? "" : " Gate Points: [" + logGateInfo + "]"));
            mockCpuLogger.log(Util.getLevelAsString(layoutModified));
            if(Util.isWordOnEndzone(level.getLevelStaticData().getLayoutOriginal(), answer) != null){
                log(difficulty, player, level.getLevelStaticData().getId(), String.format("Level complete (Points: %s, Words: %s)\n%s\n%s",
                        sessionStatistics.getTotalPoints()+"", sessionStatistics.getSubmittedWords().size()+"", Util.getLevelAsString(layoutModified).trim(), LOG_TICKS));
                break;
            }
            handleUnderlyingSpecialTiles(level, layoutModified);
            handleSubmittedWordEvent(level, layoutModified, sessionStatistics);
            handleRogueTiles(layoutModified, playedWords);
            targetXY = Util.getOptimalTargetXY(layoutModified, answer.getLastLetter().getXY());
            turn++;
        }
        levelSummaries.add(new TestedLevelSummary(level.getLevelStaticData().getId(), player, difficulty, sessionStatistics, forfeited));
        return true;
    }

    private void handleUnderlyingSpecialTiles(Level level, ArrayList<String> layoutModified){
        int[] xyCoveredDetonatory = Util.isWordOnDetonator(level.getLevelStaticData().getLayoutOriginal(), answer);
        if(xyCoveredDetonatory != null){
            for(int i = 0; i < level.getLevelStaticData().getDetonatorLinks().size(); i++){
                DetonatorLink link = level.getLevelStaticData().getDetonatorLinks().get(i);
                if(Util.equal(xyCoveredDetonatory, link.getXyDetonator())){
                    //Change crates to maptiles
                    for(int[] xyCrate : link.getXyCrates()){
                        Util.updateLayout(level.getLevelStaticData().getLayoutOriginal(),
                                layoutModified, xyCrate, TileCodes.MapTile, false);
                    }
                }
            }
        }
    }

    private void handleSubmittedWordEvent(Level level, ArrayList<String> layoutModified, SessionStatistics sessionStatistics) {
        for(GateLockPoints g : level.getLevelStaticData().getPointsLocks()){
            if(sessionStatistics.getTotalPoints() >= g.getOriginalPoints()){
                if(Util.getCharacterAt(layoutModified, g.getXYGate()).equals(TileCodes.Gate.getCharacter())){
                    Util.updateLayout(level.getLevelStaticData().getLayoutOriginal(), layoutModified, g.getXYGate(),
                            TileCodes.MapTile, false);
                }
            }
        }
    }

    private void handleRogueTiles(ArrayList<String> layoutModified, ArrayList<HintWord> playedWords) {
        for(HintLetter l : answer.getLetters()){
            ArrayList<int[]> touchingRogueTiles = Util.getXYsOfTouchingRogueTilesRecursively(layoutModified, l.getXY());
            if(touchingRogueTiles != null && !touchingRogueTiles.isEmpty()){
                for(int[] xyRogueTile : touchingRogueTiles){
                    ArrayList<HintWord> words = Util.getWordsOfBoardLetter(databaseGateway, layoutModified, xyRogueTile, answer);
                    for(HintWord word : words){
                        if(!playedWords.contains(word)){
                            playedWords.add(word);
                        }
                    }
                }
            }
        }
    }

    private void log(Difficulty difficulty, PlayerType playerType, int levelId, String msg){
        mockCpuLogger.log(String.format("[%s | %s | %s]: %s",
                difficulty.name(),
                playerType.name(),
                levelId+"",
                msg));
    }

    private void logLevelTitle(Level level, Difficulty difficulty, PlayerType player){
        String title = "Simulating level [" + level.getLevelStaticData().getId() + "] on " + difficulty.name() + " for " + player.name();
        mockCpuLogger.log(Util.getBlockWithText('*', title));
        mockCpuLogger.log(Util.getLevelAsString(level.getLevelStaticData().getLayoutOriginal()).trim());
    }

    private void logTestPreview(int levelToTest, ArrayList<Level> levels, ArrayList<Difficulty> difficultiesToTest, ArrayList<PlayerType> playersToTest){
        mockCpuLogger.log("Testing " + (levelToTest < 1 ? ("[" + levels.size() + "] levels") : ("level [" + levelToTest + "]") ));

        mockCpuLogger.log("Testing on " + difficultiesToTest.size() + " difficulties:");
        for(Difficulty d : difficultiesToTest){
            mockCpuLogger.log(" - " + d.name());
        }

        mockCpuLogger.log("Testing for " + playersToTest.size() + " player types:");
        for(PlayerType p : playersToTest){
            mockCpuLogger.log(" - " + p.name());
        }

        logBreak();
    }

    private void logTestResults() {
        StringBuilder sb = new StringBuilder();
        mockCpuLogger.setCurrentTestTag("Final Summary");
        mockCpuLogger.log(Util.getBlockWithText('~', "End of test summary"));
        int prevLevel = 0;
        for(TestedLevelSummary l : levelSummaries){
            if (l.getId() != prevLevel){
                prevLevel = l.getId();
                sb.append("\n\nLevel " + String.format("%-2s", l.getId()) + " Difficulty Status    | Points | Turns | AvgPoints | AvgLength | All words played");
            }
            sb.append(String.format("\n(%s | %s) %s | %s | %s | %s | %s | %s",
                    l.getPlayerType().name(),
                    String.format("%-10s", l.getDifficulty().name()),
                    l.isForfeited() ? "FORFEITED" : "completed",
                    String.format("%-6s", l.getSessionStatistics().getTotalPoints()),
                    String.format("%-5s", l.getSessionStatistics().getSubmittedWords().size()),
                    String.format("%-9s", new DecimalFormat("0.0").format(l.getSessionStatistics().getAverageWordPoints())),
                    String.format("%-9s", new DecimalFormat("0.0").format(l.getSessionStatistics().getAverageWordLength())),
                    Util.join(", ", l.getSessionStatistics().getSubmittedWords())));
        }
        mockCpuLogger.log(sb.toString());
    }

    private void logBreak(){
        for(int i = 0; i < 3; i++) {
            mockCpuLogger.log("");
        }
    }

    //endregion

    @Override
    public boolean isTestAssertionEnabled() {
        return false;
    }

    @Override
    public boolean isPrintTestResultsEnabled() {
        return false;
    }

    @Override
    public boolean isLogCpuLoggerViaSystemOutPrintEnabled() {
        return LOG_CPU_LOGGER_LOGIC;
    }

    @Override
    public boolean isLogTextFileToDirEnabled() {
        return LOG_TEXT_FILE_TO_DIR;
    }

    @Test
    @Ignore //Run this to find the output from querying the dictionary db (Im using this for debugging)
    public void testRawSQLagainstDictionaryDb(){
        String sql = "SELECT 2, Word, DisplayWord, Frequency FROM Dictionary WHERE Word IN ('yearsp', 'yearsa', 'yearsr', 'yearss', 'yearsd', 'yearst', 'yearse', 'yearsi', 'yearso') \n" +
                "UNION\n" +
                "SELECT 4, Word, DisplayWord, Frequency FROM Dictionary WHERE Word IN ('p', 'a', 'r', 's', 'd', 't', 'e', 'i', 'o')";
        ArrayList<DictionaryEntry> output = databaseGateway.getDictionaryEntries(sql);
        mockCpuLogger.log("--- Output from testRawSQLagainstDictionaryDb ---");
        mockCpuLogger.log("Found " + output.size() + " words...");
        for(DictionaryEntry d : output){
            mockCpuLogger.log(" - " + d.getWord());
        }
    }
}

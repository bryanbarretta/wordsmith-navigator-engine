package bryans.work.wordsmithnavigatorengine;

import org.junit.Test;

import java.util.ArrayList;

import bryans.work.wordsmithnavigatorengine.enums.Difficulty;
import bryans.work.wordsmithnavigatorengine.enums.TileCodes;
import bryans.work.wordsmithnavigatorengine.objects.HintLetter;
import bryans.work.wordsmithnavigatorengine.objects.HintWord;
import bryans.work.wordsmithnavigatorengine.superclass.BaseWETest;
import bryans.work.wordsmithnavigatorengine.util.Util;

import static junit.framework.TestCase.assertTrue;

public class TargetDetonatorTest extends BaseWETest {

    @Override
    public boolean isTestAssertionEnabled() {
        return false;
    }

    @Override
    public boolean isPrintTestResultsEnabled() {
        return false;
    }

    @Override
    public boolean isLogCpuLoggerViaSystemOutPrintEnabled() {
        return false;
    }

    @Override
    public boolean isLogTextFileToDirEnabled() {
        return false;
    }

    @Test
    public void testLevelTrustIssuesPart1() throws InterruptedException {
        //Test CPU is more aggressive when aiming for a detonator
        testLevelTrustIssuesPart1(Difficulty.VERY_EASY, true, "shame", "er");
        testLevelTrustIssuesPart1(Difficulty.EASY,      true, "laugh", "spare");
        testLevelTrustIssuesPart1(Difficulty.NORMAL,    true, "shame", "er");
        testLevelTrustIssuesPart1(Difficulty.HARD,      true, "shame", "er");
        testLevelTrustIssuesPart1(Difficulty.VERY_HARD, true, "shame", "er");
        testLevelTrustIssuesPart1(Difficulty.IMPOSSIBLE,true, "shame", "er");
        //Test CPU is less aggressive when aiming for the endzone
        testLevelTrustIssuesPart1(Difficulty.VERY_EASY, false, "eye", "are");
        testLevelTrustIssuesPart1(Difficulty.EASY,      false, "laugh", "gas");
        testLevelTrustIssuesPart1(Difficulty.NORMAL,    false, "enough", "gas");
        testLevelTrustIssuesPart1(Difficulty.HARD,      false, "shame", "eleanor");
        testLevelTrustIssuesPart1(Difficulty.VERY_HARD, false, "shame", "er");
        testLevelTrustIssuesPart1(Difficulty.IMPOSSIBLE,false, "shame", "er");
    }

    @Test
    public void testLevelTrustIssuesPart2() throws InterruptedException {
        //testLevelTrustIssuesPart2(Difficulty.VERY_EASY, new String[]{"test"});
        //testLevelTrustIssuesPart2(Difficulty.EASY,      new String[]{"test"});
        testLevelTrustIssuesPart2(Difficulty.NORMAL,    new String[]{"test"});
        //testLevelTrustIssuesPart2(Difficulty.HARD,      new String[]{"test"});
        //testLevelTrustIssuesPart2(Difficulty.VERY_HARD, new String[]{"test"});
        //testLevelTrustIssuesPart2(Difficulty.IMPOSSIBLE,new String[]{"test"});
    }

    private void testLevelTrustIssuesPart1(Difficulty difficulty, boolean targetIsDetonator, String expectedAnswer1, String expectedAnswer2) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        Character targetChar = targetIsDetonator ? TileCodes.Detonator.getCharacter() : TileCodes.EndZone.getCharacter();

        layout.add("patient-clueless");
        layout.add("0......-.......$");
        layout.add(".......-........");
        layout.add(".......-.......-");
        layout.add(".....++----...." + targetChar);
        layout.add(".....+^-1+-.....");
        layout.add(".....++-++-.....");
        layout.add(".......-........");
        layout.add(".......-........");
        layout.add(".......-........");

        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(8, 0, 'c', true, false));
        playedWord.addLetter(new HintLetter(9, 0, 'l', true, false));
        playedWord.addLetter(new HintLetter(10, 0, 'u', true, false));
        playedWord.addLetter(new HintLetter(11, 0, 'e', true, false));
        playedWord.addLetter(new HintLetter(12, 0, 'l', true, false));
        playedWord.addLetter(new HintLetter(13, 0, 'e', true, false));
        playedWord.addLetter(new HintLetter(14, 0, 's', true, false));
        playedWord.addLetter(new HintLetter(15, 0, 's', true, false));
        final HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                Util.getFirstXYof(layout, targetChar),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer1));

        //2nd Word

        ArrayList<HintWord> playedWords = new ArrayList<HintWord>(){{add(playedWord); add(answer);}};
        layout = Util.applyHintWordToLevel(layout, answer);
        hudLetters = "lfoneosarp";
        HintWord answer2 = getNextCPUWord(playedWords,
                layout,
                Util.getFirstXYof(layout, targetChar),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer2);
        if(isTestAssertionEnabled())
            assertTrue(answer2.getDictionaryEntry().getWord().equals(expectedAnswer2));
    }

    private void testLevelTrustIssuesPart2(Difficulty difficulty, String[] expectedAnswers) throws InterruptedException {

        HintWord answer = null;

        ArrayList<String> layout = new ArrayList<>();

        layout.add("patient-clueless");
        layout.add("0......-......h.");
        layout.add(".......-......a.");
        layout.add(".......-......m-");
        layout.add(".....++----...er");
        layout.add(".....+^-1.-.....");
        layout.add(".....++-..-.....");
        layout.add("1111111-........");
        layout.add("1111111-........");
        layout.add("1111111-........");

        String hudLetters = "eayhuglmno";

        ArrayList<HintWord> playedWords = new ArrayList(){{
            add(new HintWord(new ArrayList<HintLetter>(){{
                add(new HintLetter(8, 0, 'c', true, false));
                add(new HintLetter(9, 0, 'l', true, false));
                add(new HintLetter(10, 0, 'u', true, false));
                add(new HintLetter(11, 0, 'e', true, false));
                add(new HintLetter(12, 0, 'l', true, false));
                add(new HintLetter(13, 0, 'e', true, false));
                add(new HintLetter(14, 0, 's', true, false));
                add(new HintLetter(15, 0, 's', true, false));
            }}));
            add(new HintWord(new ArrayList<HintLetter>(){{
                add(new HintLetter(14, 0, 's', true, false));
                add(new HintLetter(14, 1, 'h', true, false));
                add(new HintLetter(14, 2, 'a', true, false));
                add(new HintLetter(14, 3, 'm', true, false));
                add(new HintLetter(14, 4, 'e', true, false));
            }}));
            add(new HintWord(new ArrayList<HintLetter>(){{
                add(new HintLetter(14, 4, 'e', true, false));
                add(new HintLetter(15, 4, 'r', true, false));
            }}));
        }};

        boolean hasReachedTarget = false;
        int iAnswer = 0;
        while (!hasReachedTarget) {
            if(answer != null){
                playedWords.add(answer);
                layout = Util.applyHintWordToLevel(layout, answer);
                hudLetters = "aeiongstrl";
            }
            answer = null;
            int[] targetXY = Util.getOptimalTargetXY(layout, playedWords.get(playedWords.size() - 1).getLetters()
                    .get(playedWords.get(playedWords.size() - 1).getLetters().size() - 1).getXY());
            answer = getNextCPUWord(playedWords, layout, targetXY, hudLetters, difficulty);
            if(answer == null){
                System.out.println("! Cannot think of a word => Forfeit !");
                break;
            }
            if (isPrintTestResultsEnabled()) {
                log(difficulty, answer);
                System.out.println(Util.getLevelAsString(layout) + "\n");
            }
            if (isTestAssertionEnabled()) {
                assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswers[iAnswer]));
                iAnswer++;
            }
            break;
        }
        //System.out.println(LevelUtils.getLevelAsString(layout) + "\n");
    }

}
